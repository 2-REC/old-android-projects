package derek.android.systemquery.components;

import android.app.Activity;
import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import derek.android.systemquery.R;


public class LayoutSensorView extends RelativeLayout
{
    private TextView mTextSensorType;

    private LayoutSingleLine mLayoutSensorAvailable;

    private LayoutSingleLine mLayoutSensorValue0;
    private LayoutSingleLine mLayoutSensorValue1;
    private LayoutSingleLine mLayoutSensorValue2;

//// remove 2 - begin
/*
    private LayoutRadioLine mLayoutDelay;
*/
//// remove 2 - end


    private boolean mAvailable = false;


    public LayoutSensorView(Context context)
    {
        super(context);
//// test 1 - mid
        setupViewItems();
//// test 1 - end
    }

    public LayoutSensorView(Context context, AttributeSet attrs)
    {
        super(context, attrs);
//// test 1 - mid
        setupViewItems();
//// test 1 - end
    }

    public LayoutSensorView(Context context, AttributeSet attrs, int defStyle)
    {
        super(context, attrs, defStyle);
//// test 1 - mid
        setupViewItems();
//// test 1 - end
    }

//// test 1 - begin
/*
    @Override
    protected void onFinishInflate()
    {
        super.onFinishInflate();
        setupViewItems();
    }
*/
//// test 1 - mid


    private void setupViewItems()
    {
        ((Activity)getContext()).getLayoutInflater().inflate(R.layout.layout_sensor_view, this);

        mTextSensorType = (TextView) findViewById(R.id.text_sensor_name);
        mLayoutSensorAvailable = (LayoutSingleLine) findViewById(R.id.layout_sensor_available);
        mLayoutSensorAvailable.setTextLeft("Available:");
        mLayoutSensorAvailable.setTextRight("NO");

        mLayoutSensorValue0 = (LayoutSingleLine) findViewById(R.id.layout_sensor_value_0);
        mLayoutSensorValue0.setTextLeft("value 0 :");
        mLayoutSensorValue0.setVisibility(View.GONE);
        mLayoutSensorValue1 = (LayoutSingleLine) findViewById(R.id.layout_sensor_value_1);
        mLayoutSensorValue1.setTextLeft("value 1 :");
        mLayoutSensorValue1.setVisibility(View.GONE);
        mLayoutSensorValue2 = (LayoutSingleLine) findViewById(R.id.layout_sensor_value_2);
        mLayoutSensorValue2.setTextLeft("value 2 :");
        mLayoutSensorValue2.setVisibility(View.GONE);

//// remove 2 - begin
/*
        mLayoutDelay = (LayoutRadioLine) findViewById(R.id.layout_sensor_delay);
        mLayoutDelay.setVisibility(View.GONE);
        mLayoutDelay.setTextTitle("Delay:");
        mLayoutDelay.setTextRadio(1, "NORMAL");
        mLayoutDelay.setTextRadio(2, "GAME");
        mLayoutDelay.setTextRadio(3, "FASTEST");
*/
//// remove 2 - end
    }

    public void setTextSensorType(String text)
    {
        mTextSensorType.setText(text);
    }

    public void setSensorAvailable(boolean available)
    {
        mAvailable = available;
        if (mAvailable)
        {
            mLayoutSensorAvailable.setTextRight("YES");
            mLayoutSensorValue0.setVisibility(View.VISIBLE);
            mLayoutSensorValue1.setVisibility(View.VISIBLE);
            mLayoutSensorValue2.setVisibility(View.VISIBLE);
//// remove 2 - begin
/*
            mLayoutDelay.setVisibility(View.VISIBLE);
*/
//// remove 2 - end
        }
        else
        {
            mLayoutSensorAvailable.setTextRight("NO");
            mLayoutSensorValue0.setVisibility(View.GONE);
            mLayoutSensorValue1.setVisibility(View.GONE);
            mLayoutSensorValue2.setVisibility(View.GONE);
//// remove 2 - begin
/*
            mLayoutDelay.setVisibility(View.GONE);
*/
//// remove 2 - end
        }
    }

    public void updateSensorValues(float v0, float v1, float v2)
    {
        if (mAvailable)
        {
            mLayoutSensorValue0.setTextRight(Float.toString(v0));
            mLayoutSensorValue1.setTextRight(Float.toString(v1));
            mLayoutSensorValue2.setTextRight(Float.toString(v2));
        }
    }

//// remove 2 - begin
/*
    public RadioGroup getRadioGroup()
    {
        return mLayoutDelay.getRadioGroup();
    }
*/
//// remove 2 - end

}
