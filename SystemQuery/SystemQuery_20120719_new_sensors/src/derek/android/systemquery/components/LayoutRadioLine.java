package derek.android.systemquery.components;

import android.app.Activity;
import android.content.Context;
import android.util.AttributeSet;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import derek.android.systemquery.R;

// !!!! ???? TODO : should handle the Radio buttons selsction here ???? !!!!
// (instead of in Activity)
// => would be more logical, but then would need to have a link to the Activity ...
// (but then would only have to tell the Activity the index of selected Radio Button)


public class LayoutRadioLine extends LinearLayout
{
    private TextView mTextTitle;
    private RadioButton mRadio1;
    private RadioButton mRadio2;
    private RadioButton mRadio3;

    private RadioGroup mRadioGroup;


    public LayoutRadioLine(Context context)
    {
        super(context);

//// test 1 - mid
        setupViewItems();
//// test 1 - end
    }

    public LayoutRadioLine(Context context, AttributeSet attrs)
    {
        super(context, attrs);

//// test 1 - mid
        setupViewItems();
//// test 1 - end
    }

/*
    public LayoutRadioLine(Context context, AttributeSet attrs, int defStyle)
    {
        super(context, attrs, defStyle);

//// test 1 - mid
        setupViewItems();
//// test 1 - end
    }
*/

//// test 1 - begin
/*
    @Override
    protected void onFinishInflate()
    {
        super.onFinishInflate();
        setupViewItems();
    }
*/
//// test 1 - mid


    private void setupViewItems()
    {
        ((Activity)getContext()).getLayoutInflater().inflate(R.layout.layout_radio_line, this);
        mTextTitle = (TextView) findViewById(R.id.text_title);
        mRadioGroup = (RadioGroup) findViewById(R.id.radio_group);
        mRadio1 = (RadioButton) findViewById(R.id.radio_1);
        mRadio2 = (RadioButton) findViewById(R.id.radio_2);
        mRadio3 = (RadioButton) findViewById(R.id.radio_3);
    }

    public void setTextTitle(String text)
    {
        mTextTitle.setText(text);
    }

    public void setTextRadio(int index, String text)
    {
        switch(index)
        {
        case 1:
            mRadio1.setText(text);
            break;
        case 2:
            mRadio2.setText(text);
            break;
        case 3:
            mRadio3.setText(text);
            break;
        }
    }

    public RadioGroup getRadioGroup()
    {
        return mRadioGroup;
    }

}
