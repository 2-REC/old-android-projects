package derek.android.systemquery;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.Context;
import android.content.pm.ConfigurationInfo;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

// !!!! ???? TODO : what to import ???? !!!!
//? import javax.microedition.khronos.egl.EGLConfig;
//? import javax.microedition.khronos.opengles.GL10;
//! import android.opengl.GLES20;
//? import android.opengl;

import android.util.Log;


public class OpenGLActivity extends Activity
{
    private static final String tag = "LOG_INIT";

// !!!! TODO : get more info (capabilities, ...) !!!!
    private TextView mTextOpenGL;


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

// !!!! TODO : should have a title ... !!!!
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        setContentView(R.layout.opengl);

        mTextOpenGL = (TextView) findViewById(R.id.text_opengl_version);

//////// - begin
/*
        ActivityManager am = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        ConfigurationInfo info = am.getDeviceConfigurationInfo();
        mTextOpenGL.setText(String.valueOf(info.reqGlEsVersion));
*/
//////// - mid

        String version = gl.glGetString(GL10.GL_VERSION);
        mTextOpenGL.setText(version);

        String extensions = " " + gl.glGetString(GL10.GL_EXTENSIONS) + " ";
        String renderer = gl.glGetString(GL10.GL_RENDERER);
        boolean isSoftwareRenderer = renderer.contains("PixelFlinger");

Log.d(tag, "OpenGLActivity - extensions : " + extensions);
Log.d(tag, "OpenGLActivity - version : " + version);
Log.d(tag, "OpenGLActivity - renderer : " + renderer);
if (isSoftwareRenderer)
    Log.d(tag, "OpenGLActivity - isSoftwareRenderer : YES");
else
    Log.d(tag, "OpenGLActivity - isSoftwareRenderer : NO");

        // On 1.6 and newer, we could use ActivityManager.getDeviceConfigurationInfo() to get the GL version.
        // To include 1.5, I'll use the GL version string.
        boolean isOpenGL10 = version.contains(" 1.0");
        boolean supportsDrawTexture = extensions.contains(" GL_OES_draw_texture "); // draw_texture extension
        boolean supportsETC1 = extensions.contains(" GL_OES_compressed_ETC1_RGB8_texture "); // standard ETC1 support extension

        // VBOs are guaranteed in GLES1.1, but they were an extension under 1.0.
        // There's no point in using VBOs when using the software renderer (though they are supported).
        boolean supportsVBOs =
            !isSoftwareRenderer && (!isOpenGL10 || extensions.contains("vertex_buffer_object "));

if (isOpenGL10)
    Log.d(tag, "OpenGLActivity - isOpenGL10 : YES");
else
    Log.d(tag, "OpenGLActivity - isOpenGL10 : NO");

if (supportsDrawTexture)
    Log.d(tag, "OpenGLActivity - supportsDrawTexture : YES");
else
    Log.d(tag, "OpenGLActivity - supportsDrawTexture : NO");

if (supportsETC1)
    Log.d(tag, "OpenGLActivity - supportsETC1 : YES");
else
    Log.d(tag, "OpenGLActivity - supportsETC1 : NO");

if (supportsVBOs)
    Log.d(tag, "OpenGLActivity - supportsVBOs : YES");
else
    Log.d(tag, "OpenGLActivity - supportsVBOs : NO");

//////// - end

    }


////////
// !!!! TODO : can also try this ... !!!!
/*

private boolean checkGL20Support( Context context ) 
{ 
    EGL10 egl = (EGL10) EGLContext.getEGL();        
    EGLDisplay display = egl.eglGetDisplay(EGL10.EGL_DEFAULT_DISPLAY); 
 
    int[] version = new int[2]; 
    egl.eglInitialize(display, version); 
 
    int EGL_OPENGL_ES2_BIT = 4; 
    int[] configAttribs = 
    { 
        EGL10.EGL_RED_SIZE, 4, 
        EGL10.EGL_GREEN_SIZE, 4, 
        EGL10.EGL_BLUE_SIZE, 4, 
        EGL10.EGL_RENDERABLE_TYPE, EGL_OPENGL_ES2_BIT, 
        EGL10.EGL_NONE 
    }; 
 
    EGLConfig[] configs = new EGLConfig[10]; 
    int[] num_config = new int[1]; 
    egl.eglChooseConfig(display, configAttribs, configs, 10, num_config);      
    egl.eglTerminate(display); 
    return num_config[0] > 0; 
}

*/
////////

}
