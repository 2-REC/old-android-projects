package derek.android.systemquery;

import android.content.Context;
import org.openintents.sensorsimulator.hardware.Sensor;
import org.openintents.sensorsimulator.hardware.SensorEvent;
import org.openintents.sensorsimulator.hardware.SensorEventListener;
import org.openintents.sensorsimulator.hardware.SensorManagerSimulator;
import java.util.List;


public class SensorsHandler implements SensorEventListener
{
    public static final int DELAY_NORMAL = SensorManagerSimulator.SENSOR_DELAY_NORMAL;
    public static final int DELAY_GAME = SensorManagerSimulator.SENSOR_DELAY_GAME;
    public static final int DELAY_FASTEST = SensorManagerSimulator.SENSOR_DELAY_FASTEST;

    private SensorManagerSimulator mSensorManager = null;

    private SensorsActivity mParentActivity = null;

    private boolean mAccelerometer = false;
    private boolean mOrientation = false;

    int mAccelerometerDelay = DELAY_NORMAL;
    int mOrientationDelay = DELAY_NORMAL;


    public SensorsHandler(Context c)
    {
        mParentActivity = (SensorsActivity)c;
        mSensorManager = SensorManagerSimulator.getSystemService(mParentActivity, Context.SENSOR_SERVICE);
        mSensorManager.connectSimulator();
    }

    public boolean addAccelerometer()
    {
        if (mSensorManager != null)
        {
            if (!mAccelerometer)
            {
                Sensor accelerometer = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
                if (accelerometer != null)
                {
                    mAccelerometer = true;
//? maximumRange = accelerometer.getMaximumRange();
                }
            }
        }
        return mAccelerometer;
    }
    public void removeAccelerometer()
    {
        mAccelerometer = false;
//? maximumRange = 0; ????
    }

    public boolean addOrientation()
    {
        if (mSensorManager != null)
        {
            if (!mOrientation)
            {
                Sensor orientation = mSensorManager.getDefaultSensor(Sensor.TYPE_ORIENTATION);
                if (orientation != null)
                {
                    mOrientation = true;
//? maximumRange = orientation.getMaximumRange();
                }
            }
        }
        return mOrientation;
    }
    public void removeOrientation()
    {
        mOrientation = false;
//? maximumRange = 0; ????
    }


    public boolean sensorsAvailable()
    {
        return (mSensorManager != null);
    }

    public int sensorsNb()
    {
        if (mSensorManager == null)
            return -1;

        List<Integer> sensorsList;
        sensorsList = mSensorManager.getSensors();

        return sensorsList.size();
    }

    public boolean accelerometerAvailable()
    {
        return mAccelerometer;
    }

    public boolean orientationAvailable()
    {
        return mOrientation;
    }

    public void setAccelerometerDelay(int delay)
    {
        mAccelerometerDelay = delay;
    }

    public void setOrientationDelay(int delay)
    {
        mOrientationDelay = delay;
    }

    public void registerListeners()
    {
        if (mSensorManager != null)
        {
            if (mAccelerometer)
            {
                mSensorManager.registerListener(this,
                                                mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER),
                                                mAccelerometerDelay);
            }

            if (mOrientation)
            {
                mSensorManager.registerListener(this,
                                                mSensorManager.getDefaultSensor(Sensor.TYPE_ORIENTATION),
                                                mOrientationDelay);
            }
        }
    }

    public void unregisterListeners()
    {
        if (mSensorManager != null)
        {
            mSensorManager.unregisterListener(this);
        }
    }

    public void release()
    {
        mSensorManager.disconnectSimulator();

// !!!! ???? TODO : OK ???? !!!!
        removeAccelerometer();
        removeOrientation();
        mSensorManager = null;
        mParentActivity = null;
    }


    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy)
    {
    }

    @Override
    public void onSensorChanged(SensorEvent event)
    {
        /*
         * event.values[0]: azimuth, rotation around the Z axis.
         * event.values[1]: pitch, rotation around the X axis.
         * event.values[2]: roll, rotation around the Y axis.
         */

        switch (event.type)
        {
        case Sensor.TYPE_ACCELEROMETER:
            mParentActivity.updateAccelerometer(event.values[0],
                                                event.values[1],
                                                event.values[2]);
            break;
        case Sensor.TYPE_ORIENTATION:
            mParentActivity.updateOrientation(event.values[0],
                                              event.values[1],
                                              event.values[2]);
            break;
        }
    }

}
