package derek.android.systemquery;

import java.util.List;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.util.Log;


public class SensorsHandler implements SensorEventListener
{
    private static final String tag = "LOG_INIT";

    public static final int DELAY_NORMAL = SensorManager.SENSOR_DELAY_NORMAL;
    public static final int DELAY_GAME = SensorManager.SENSOR_DELAY_GAME;
    public static final int DELAY_FASTEST = SensorManager.SENSOR_DELAY_FASTEST;

    private SensorManager mSensorManager = null;

    private SensorsActivity mParentActivity = null;

    private Sensor mSensorAccelerometer = null;
    private Sensor mSensorOrientation = null;

    private boolean mSensorAccelerometerRegistered = false;
    private boolean mSensorOrientationRegistered = false;

    int mAccelerometerDelay = DELAY_NORMAL;
    int mOrientationDelay = DELAY_NORMAL;


    public SensorsHandler(Context c)
    {
        mParentActivity = (SensorsActivity)c;
        mSensorManager = (SensorManager)mParentActivity.getSystemService(Context.SENSOR_SERVICE);
    }

    public void addAccelerometer()
    {
        if (mSensorManager != null)
        {
            if (mSensorAccelerometer == null)
            {
                mSensorAccelerometer = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
//? maximumRange = mSensorAccelerometer.getMaximumRange();
            }
        }
    }
    public void removeAccelerometer()
    {
        if (mSensorManager != null)
        {
            if (mSensorAccelerometer != null)
            {
                if (mSensorAccelerometerRegistered)
                {
                    mSensorManager.unregisterListener(this, mSensorAccelerometer);
                    mSensorAccelerometerRegistered = false;
                }
                mSensorAccelerometer = null;
//? maximumRange = 0; ????
            }
        }
    }

    public void addOrientation()
    {
        if (mSensorManager != null)
        {
            if (mSensorOrientation == null)
            {
                mSensorOrientation = mSensorManager.getDefaultSensor(Sensor.TYPE_ORIENTATION);
//? maximumRange = mSensorOrientation.getMaximumRange();
            }
        }
    }
    public void removeOrientation()
    {
        if (mSensorManager != null)
        {
            if (mSensorOrientation != null)
            {
                if (mSensorOrientationRegistered)
                {
                    mSensorManager.unregisterListener(this, mSensorOrientation);
                    mSensorOrientationRegistered = false;
                }
                mSensorOrientation = null;
//? maximumRange = 0; ????
            }
        }
    }


    public boolean sensorsAvailable()
    {
        return (mSensorManager != null);
    }

    public int sensorsNb()
    {
        if (mSensorManager == null)
            return -1;

        List<Sensor> sensorsList;
        sensorsList = mSensorManager.getSensorList(Sensor.TYPE_ALL);

        return sensorsList.size();
    }

    public boolean accelerometerAvailable()
    {
        return (mSensorAccelerometer != null);
    }

    public boolean orientationAvailable()
    {
        return (mSensorOrientation != null);
    }

    public void setAccelerometerDelay(int delay)
    {
        mAccelerometerDelay = delay;

        if (mSensorAccelerometer != null)
        {
            if (mSensorAccelerometerRegistered)
            {
                mSensorManager.unregisterListener(this, mSensorAccelerometer);
                mSensorAccelerometerRegistered = mSensorManager.registerListener(this, mSensorAccelerometer, mAccelerometerDelay);
            }
        }
    }

    public void setOrientationDelay(int delay)
    {
        mOrientationDelay = delay;

        if (mSensorOrientation != null)
        {
            if (mSensorOrientationRegistered)
            {
                mSensorManager.unregisterListener(this, mSensorOrientation);
                mSensorOrientationRegistered = mSensorManager.registerListener(this, mSensorOrientation, mOrientationDelay);
            }
        }
    }

    public void registerListeners()
    {
        if (mSensorManager != null)
        {
            if (mSensorAccelerometer != null)
            {
                if (!mSensorAccelerometerRegistered)
                {
                    mSensorAccelerometerRegistered = mSensorManager.registerListener(this, mSensorAccelerometer, mAccelerometerDelay);
                }
            }

            if (mSensorOrientation != null)
            {
                if (!mSensorOrientationRegistered)
                {
                    mSensorOrientationRegistered = mSensorManager.registerListener(this, mSensorOrientation, mOrientationDelay);
                }
            }
        }
    }

    public void unregisterListeners()
    {
        if (mSensorManager != null)
        {
            mSensorManager.unregisterListener(this);
            mSensorAccelerometerRegistered = false;
            mSensorOrientationRegistered = false;
        }
    }

    public void release()
    {
// !!!! ???? TODO : OK ???? !!!!
        removeAccelerometer();
        removeOrientation();
        mSensorManager = null;
        mParentActivity = null;
    }


    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy)
    {
        switch (sensor.getType())
        {
        case Sensor.TYPE_ACCELEROMETER:
            Log.d(tag, "onAccuracyChanged - ACCELEROMETER : accuracy: " + accuracy);
            break;
        case Sensor.TYPE_ORIENTATION:
            Log.d(tag, "onAccuracyChanged - ORIENTATION : accuracy: " + accuracy);
            break;
        }
    }

    @Override
    public void onSensorChanged(SensorEvent event)
    {
        /*
         * event.values[0]: azimuth, rotation around the Z axis.
         * event.values[1]: pitch, rotation around the X axis.
         * event.values[2]: roll, rotation around the Y axis.
         */

        switch (event.sensor.getType())
        {
        case Sensor.TYPE_ACCELEROMETER:
            mParentActivity.updateAccelerometer(event.values[0],
                                                event.values[1],
                                                event.values[2]);
            break;
        case Sensor.TYPE_ORIENTATION:
            mParentActivity.updateOrientation(event.values[0],
                                              event.values[1],
                                              event.values[2]);
            break;
        }
    }

}
