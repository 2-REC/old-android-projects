package derek.android.systemquery;

import android.app.Activity;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

public class BuildInfoActivity extends Activity
{
    private TextView mTextBuildVersionSDKINT;
    private TextView mTextBuildVersionCodeName;
    private TextView mTextBuildVersionIncremental;
    private TextView mTextBuildVersionRelease;
    private TextView mTextBuildBrand;
    private TextView mTextBuildCPUABI;
    private TextView mTextBuildCPUABI2;
    private TextView mTextBuildDevice;
    private TextView mTextBuildModel;
    private TextView mTextBuildManufacturer;
    private TextView mTextBuildProduct;
    private TextView mTextBuildUser;


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

// !!!! TODO : should have a title ... !!!!
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        setContentView(R.layout.build_info);

        mTextBuildVersionSDKINT = (TextView) findViewById(R.id.text_ver_sdkint);
        mTextBuildVersionCodeName = (TextView) findViewById(R.id.text_ver_codename);
        mTextBuildVersionIncremental = (TextView) findViewById(R.id.text_ver_inc);
        mTextBuildVersionRelease = (TextView) findViewById(R.id.text_ver_release);
        mTextBuildModel = (TextView) findViewById(R.id.text_model);
        mTextBuildManufacturer = (TextView) findViewById(R.id.text_manuf);
        mTextBuildProduct = (TextView) findViewById(R.id.text_product);
        mTextBuildBrand = (TextView) findViewById(R.id.text_brand);
        mTextBuildCPUABI = (TextView) findViewById(R.id.text_cpuabi);
        mTextBuildCPUABI2 = (TextView) findViewById(R.id.text_cpuabi2);
        mTextBuildDevice = (TextView) findViewById(R.id.text_device);
        mTextBuildUser = (TextView) findViewById(R.id.text_user);

        mTextBuildVersionSDKINT.setText(String.valueOf(android.os.Build.VERSION.SDK_INT));
        mTextBuildVersionCodeName.setText(android.os.Build.VERSION.CODENAME);
        mTextBuildVersionIncremental.setText(android.os.Build.VERSION.INCREMENTAL);
        mTextBuildVersionRelease.setText(android.os.Build.VERSION.RELEASE);
        mTextBuildModel.setText(android.os.Build.MODEL);
        mTextBuildManufacturer.setText(android.os.Build.MANUFACTURER);
        mTextBuildProduct.setText(android.os.Build.PRODUCT);
        mTextBuildBrand.setText(android.os.Build.BRAND);
        mTextBuildCPUABI.setText(android.os.Build.CPU_ABI);
        mTextBuildCPUABI2.setText(android.os.Build.CPU_ABI2);
        mTextBuildDevice.setText(android.os.Build.DEVICE);
        mTextBuildUser.setText(android.os.Build.USER);

    }

}
