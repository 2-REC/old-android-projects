package derek.android.systemquery;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;


public class SystemQuery extends Activity
{
    private View mButtonBuild;
    private View mButtonCpu;
    private View mButtonDisplay;
    private View mButtonSensors;
    private View mButtonGPS;
    private View mButtonOpenGL;


    private boolean mPaused;


    // Build
    private View.OnClickListener mButtonBuildListener = new View.OnClickListener()
    {
        public void onClick(View view)
        {
            if (!mPaused)
            {
                Intent intent = new Intent(getBaseContext(), BuildInfoActivity.class);
                mPaused = true;
                startActivity(intent);
            }
        }
    };

    // Cpu
    private View.OnClickListener mButtonCpuListener = new View.OnClickListener()
    {
        public void onClick(View view)
        {
            if (!mPaused)
            {
                Intent intent = new Intent(getBaseContext(), CpuInfoActivity.class);
                mPaused = true;
                startActivity(intent);
            }
        }
    };

    // Display
    private View.OnClickListener mButtonDisplayListener = new View.OnClickListener()
    {
        public void onClick(View view)
        {
            if (!mPaused)
            {
                Intent intent = new Intent(getBaseContext(), DisplayInfoActivity.class);
                mPaused = true;
                startActivity(intent);
            }
        }
    };

    // Sensors
    private View.OnClickListener mButtonSensorsListener = new View.OnClickListener()
    {
        public void onClick(View view)
        {
            if (!mPaused)
            {
                Intent intent = new Intent(getBaseContext(), SensorsActivity.class);
                mPaused = true;
                startActivity(intent);
            }
        }
    };

    // GPS
    private View.OnClickListener mButtonGPSListener = new View.OnClickListener()
    {
        public void onClick(View view)
        {
            if (!mPaused)
            {
                Intent intent = new Intent(getBaseContext(), GPSActivity.class);
                mPaused = true;
                startActivity(intent);
            }
        }
    };

    // Open GL
    private View.OnClickListener mButtonOpenGLListener = new View.OnClickListener()
    {
        public void onClick(View view)
        {
            if (!mPaused)
            {
                Intent intent = new Intent(getBaseContext(), OpenGLActivity.class);
                mPaused = true;
                startActivity(intent);
            }
        }
    };


    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.system_query);
        mPaused = true;


        mButtonBuild = findViewById(R.id.button_build);
        mButtonCpu = findViewById(R.id.button_cpu);
        mButtonDisplay = findViewById(R.id.button_display);
        mButtonSensors = findViewById(R.id.button_sensors);
        mButtonGPS = findViewById(R.id.button_gps);
        mButtonOpenGL = findViewById(R.id.button_opengl);

        mButtonBuild.setOnClickListener(mButtonBuildListener);
        mButtonCpu.setOnClickListener(mButtonCpuListener);
        mButtonDisplay.setOnClickListener(mButtonDisplayListener);
        mButtonSensors.setOnClickListener(mButtonSensorsListener);
        mButtonGPS.setOnClickListener(mButtonGPSListener);
        mButtonOpenGL.setOnClickListener(mButtonOpenGLListener);
    }

    @Override
    protected void onPause()
    {
        super.onPause();
        mPaused = true;
    }

    @Override
    protected void onResume()
    {
        super.onResume();
        mPaused = false;
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)
    {
        boolean result = true;
        if ((keyCode == KeyEvent.KEYCODE_BACK) || (keyCode == KeyEvent.KEYCODE_HOME))
        {
            finish();
        }
        else
        {
            result = super.onKeyDown(keyCode, event);
        }
        return result;
    }

}
