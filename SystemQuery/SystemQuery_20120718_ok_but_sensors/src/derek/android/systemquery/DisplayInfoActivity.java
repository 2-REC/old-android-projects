package derek.android.systemquery;

import android.app.Activity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;


public class DisplayInfoActivity extends Activity
{
    private TextView mTextScreenDensity;
    private TextView mTextScreenDensityDPI;
    private TextView mTextScreenDensityScaled;
    private TextView mTextScreenResolutionW;
    private TextView mTextScreenResolutionH;
    private TextView mTextScreenXDPI;
    private TextView mTextScreenYDPI;


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

// !!!! TODO : should have a title ... !!!!
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        setContentView(R.layout.display_info);

        mTextScreenDensityDPI = (TextView) findViewById(R.id.text_density_dpi);
        mTextScreenDensity = (TextView) findViewById(R.id.text_density);
        mTextScreenDensityScaled = (TextView) findViewById(R.id.text_density_scaled);
        mTextScreenResolutionW = (TextView) findViewById(R.id.text_resolution_width);
        mTextScreenResolutionH = (TextView) findViewById(R.id.text_resolution_height);
        mTextScreenXDPI = (TextView) findViewById(R.id.text_density_xdpi);
        mTextScreenYDPI = (TextView) findViewById(R.id.text_density_ydpi);

        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);

// !!!! TODO : make more "readable" display !!!!
// (replace some values by texts, such as "DENSITY_MEDIUM", ...)
/*
  DENSITY_DEFAULT : 160
  DENSITY_LOW : 120
  DENSITY_MEDIUM : 160
  DENSITY_HIGH : 240
  DENSITY_XHIGH : 320
  DENSITY_TV : 213
*/
        mTextScreenDensityDPI.setText(String.valueOf(metrics.densityDpi));
        mTextScreenDensity.setText(String.valueOf(metrics.density));
        mTextScreenDensityScaled.setText(String.valueOf(metrics.scaledDensity));
        mTextScreenResolutionW.setText(String.valueOf(metrics.widthPixels));
        mTextScreenResolutionH.setText(String.valueOf(metrics.heightPixels));
        mTextScreenXDPI.setText(String.valueOf(metrics.xdpi));
        mTextScreenYDPI.setText(String.valueOf(metrics.ydpi));
    }
}
