package derek.android.systemquery;

import android.content.Context;
import java.util.List;

import org.openintents.sensorsimulator.hardware.Sensor;
import org.openintents.sensorsimulator.hardware.SensorEvent;
import org.openintents.sensorsimulator.hardware.SensorEventListener;
import org.openintents.sensorsimulator.hardware.SensorManagerSimulator;


public class SensorsHandler implements SensorEventListener
{
    public static final int DELAY_NORMAL = SensorManagerSimulator.SENSOR_DELAY_NORMAL;
    public static final int DELAY_GAME = SensorManagerSimulator.SENSOR_DELAY_GAME;
    public static final int DELAY_FASTEST = SensorManagerSimulator.SENSOR_DELAY_FASTEST;

    private SensorManagerSimulator mSensorManager = null;

    private SensorsActivity mParentActivity = null;

    private Sensor mSensorAccelerometer = null;
    private Sensor mSensorOrientation = null;

    private boolean mSensorAccelerometerRegistered = false;
    private boolean mSensorOrientationRegistered = false;

    int mAccelerometerDelay = DELAY_NORMAL;
    int mOrientationDelay = DELAY_NORMAL;


    public SensorsHandler(Context c)
    {
        mParentActivity = (SensorsActivity)c;
        mSensorManager = SensorManagerSimulator.getSystemService(mParentActivity, Context.SENSOR_SERVICE);
        mSensorManager.connectSimulator();
    }

    public void addAccelerometer()
    {
        if (mSensorManager != null)
        {
            if (mSensorAccelerometer == null)
            {
                mSensorAccelerometer = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
//? maximumRange = mSensorAccelerometer.getMaximumRange();
            }
        }
    }
    public void removeAccelerometer()
    {
        if (mSensorManager != null)
        {
            if (mSensorAccelerometer != null)
            {
                if (mSensorAccelerometerRegistered)
                {
                    mSensorManager.unregisterListener(this, mSensorAccelerometer);
                    mSensorAccelerometerRegistered = false;
                }
                mSensorAccelerometer = null;
//? maximumRange = 0; ????
            }
        }
    }

    public void addOrientation()
    {
        if (mSensorManager != null)
        {
            if (mSensorOrientation == null)
            {
                mSensorOrientation = mSensorManager.getDefaultSensor(Sensor.TYPE_ORIENTATION);
//? maximumRange = mSensorOrientation.getMaximumRange();
            }
        }
    }
    public void removeOrientation()
    {
        if (mSensorManager != null)
        {
            if (mSensorOrientation != null)
            {
                if (mSensorOrientationRegistered)
                {
                    mSensorManager.unregisterListener(this, mSensorOrientation);
                    mSensorOrientationRegistered = false;
                }
                mSensorOrientation = null;
//? maximumRange = 0; ????
            }
        }
    }


    public boolean sensorsAvailable()
    {
        return (mSensorManager != null);
    }

    public int sensorsNb()
    {
        if (mSensorManager == null)
            return -1;

        List<Integer> sensorsList;
        sensorsList = mSensorManager.getSensors();

        return sensorsList.size();
    }

    public boolean accelerometerAvailable()
    {
        return (mSensorAccelerometer != null);
    }

    public boolean orientationAvailable()
    {
        return (mSensorOrientation != null);
    }

    public void setAccelerometerDelay(int delay)
    {
        mAccelerometerDelay = delay;

        if (mSensorAccelerometer != null)
        {
            if (mSensorAccelerometerRegistered)
            {
                mSensorManager.unregisterListener(this, mSensorAccelerometer);
                mSensorAccelerometerRegistered = mSensorManager.registerListener(this, mSensorAccelerometer, mAccelerometerDelay);
            }
        }
    }

    public void setOrientationDelay(int delay)
    {
        mOrientationDelay = delay;

        if (mSensorOrientation != null)
        {
            if (mSensorOrientationRegistered)
            {
                mSensorManager.unregisterListener(this, mSensorOrientation);
                mSensorOrientationRegistered = mSensorManager.registerListener(this, mSensorOrientation, mOrientationDelay);
            }
        }
    }

    public void registerListeners()
    {
        if (mSensorManager != null)
        {
            if (mSensorAccelerometer != null)
            {
                if (!mSensorAccelerometerRegistered)
                {
// !!!! TODO : use getDefaultSensor !!!!
//                    mSensorAccelerometerRegistered = mSensorManager.registerListener(this, mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER), mAccelerometerDelay);
                    mSensorAccelerometerRegistered = mSensorManager.registerListener(this, mSensorAccelerometer, mAccelerometerDelay);
                }
            }

            if (mSensorOrientation != null)
            {
                if (!mSensorOrientationRegistered)
                {
// !!!! TODO : use getDefaultSensor !!!!
//                    mSensorOrientationRegistered = mSensorManager.registerListener(this, mSensorManager.getDefaultSensor(Sensor.TYPE_ORIENTATION), mOrientationDelay);
                    mSensorOrientationRegistered = mSensorManager.registerListener(this, mSensorOrientation, mOrientationDelay);
                }
            }
        }
    }

    public void unregisterListeners()
    {
        if (mSensorManager != null)
        {
            mSensorManager.unregisterListener(this);
            mSensorAccelerometerRegistered = false;
            mSensorOrientationRegistered = false;
        }
    }

    public void release()
    {
        mSensorManager.disconnectSimulator();

// !!!! ???? TODO : OK ???? !!!!
        removeAccelerometer();
        removeOrientation();
        mSensorManager = null;
        mParentActivity = null;
    }


    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy)
    {
    }

    @Override
    public void onSensorChanged(SensorEvent event)
    {
        /*
         * event.values[0]: azimuth, rotation around the Z axis.
         * event.values[1]: pitch, rotation around the X axis.
         * event.values[2]: roll, rotation around the Y axis.
         */

        switch (event.type)
        {
        case Sensor.TYPE_ACCELEROMETER:
            mParentActivity.updateAccelerometer(event.values[0],
                                                event.values[1],
                                                event.values[2]);
            break;
        case Sensor.TYPE_ORIENTATION:
            mParentActivity.updateOrientation(event.values[0],
                                              event.values[1],
                                              event.values[2]);
            break;
        }
    }

}
