package derek.android.systemquery.components;

import android.app.Activity;
import android.content.Context;
import android.util.AttributeSet;
import android.widget.RelativeLayout;
import android.widget.TextView;
import derek.android.systemquery.R;


public class LayoutSingleLine extends RelativeLayout
{
    private TextView mTextLeft;
    private TextView mTextRight;


    public LayoutSingleLine(Context context)
    {
        super(context);
// test 1 - mid
        setupViewItems();
// test 1 - end
    }

    public LayoutSingleLine(Context context, AttributeSet attrs)
    {
        super(context, attrs);
// test 1 - mid
        setupViewItems();
// test 1 - end
    }

    public LayoutSingleLine(Context context, AttributeSet attrs, int defStyle)
    {
        super(context, attrs, defStyle);
// test 1 - mid
        setupViewItems();
// test 1 - end
    }

    @Override
    protected void onFinishInflate()
    {
        super.onFinishInflate();
// test 1 - begin
//        setupViewItems();
// test 1 - mid
    }

    private void setupViewItems()
    {
        ((Activity)getContext()).getLayoutInflater().inflate(R.layout.layout_single_line, this);
        mTextLeft = (TextView) findViewById(R.id.text_left);
        mTextRight = (TextView) findViewById(R.id.text_right);
    }

    public void setTextLeft(String text)
    {
        mTextLeft.setText(text);
    }
    public void setTextRight(String text)
    {
        mTextRight.setText(text);
    }
}
