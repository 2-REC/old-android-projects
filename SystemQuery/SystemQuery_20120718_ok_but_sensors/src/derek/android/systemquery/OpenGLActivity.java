package derek.android.systemquery;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.Context;
import android.content.pm.ConfigurationInfo;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

//? import ...khronos ?


public class OpenGLActivity extends Activity
{

// !!!! TODO : get more info (capabilities, ...) !!!!
    private TextView mTextOpenGL;


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

// !!!! TODO : should have a title ... !!!!
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        setContentView(R.layout.opengl);

        mTextOpenGL = (TextView) findViewById(R.id.text_opengl_version);
        ActivityManager am = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        ConfigurationInfo info = am.getDeviceConfigurationInfo();
        mTextOpenGL.setText(String.valueOf(info.reqGlEsVersion));

    }

}
