package derek.android.systemquery;

import android.app.Activity;
import android.os.Bundle;
//import android.util.Log;
import android.view.View;
import derek.android.systemquery.components.LayoutSensorView;
import derek.android.systemquery.components.LayoutSingleLine;


public class SensorsActivity extends Activity
{
//    private static final String tag = "LOG_INIT";

// !!!! TODO : give more info about sensors !!!!
// (separate types, names, etc.)
    private LayoutSingleLine mLayoutSensorsAvailable;
    private LayoutSingleLine mLayoutSensorsNumber;

    private LayoutSensorView mLayoutSensorAccelerometer;
    private LayoutSensorView mLayoutSensorOrientation;
//// remove 2 - begin
/*
    private RadioGroup mRadioAccelerometerDelay;
    private RadioGroup mRadioOrientationDelay;
*/
//// remove 2 - end


    // Sensors
    private SensorsHandler mSensorsHandler;
    private boolean mAccelerometer = false;
    private boolean mOrientation = false;


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

//?        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        setContentView(R.layout.sensors);

        mLayoutSensorsAvailable = (LayoutSingleLine) findViewById(R.id.layout_sensors_available);
        mLayoutSensorsAvailable.setTextLeft("Sensors availability:");

        mLayoutSensorsNumber = (LayoutSingleLine) findViewById(R.id.layout_sensors_number);
        mLayoutSensorsNumber.setTextLeft("Sensors number:");
        mLayoutSensorsNumber.setVisibility(View.GONE);

        mLayoutSensorAccelerometer = (LayoutSensorView) findViewById(R.id.sensor_view_accelerometer);
        mLayoutSensorAccelerometer.setVisibility(View.GONE);

        mLayoutSensorOrientation = (LayoutSensorView) findViewById(R.id.sensor_view_orientation);
        mLayoutSensorOrientation.setVisibility(View.GONE);


        // Sensors
        mSensorsHandler = new SensorsHandler(this);
        if (!mSensorsHandler.sensorsAvailable())
        {
            mLayoutSensorsAvailable.setTextRight("NO");
        }
        else
        {
            mLayoutSensorsAvailable.setTextRight("YES");

            // Sensors Nb
            int nb = mSensorsHandler.sensorsNb();
            mLayoutSensorsNumber.setVisibility(View.VISIBLE);
            if (nb < 0)
                mLayoutSensorsNumber.setTextRight("0 - null");
            else
                mLayoutSensorsNumber.setTextRight(String.valueOf(nb));

// !!!! TODO : should do this only if number of sensors isn't 0 !!!!
            // Accelerometer
            setAccelerometerView();

// !!!! TODO : should do this only if number of sensors isn't 0 !!!!
            // Orientation
            setOrientationView();

        }
    }


    // Accelerometer
    private void setAccelerometerView()
    {
        mLayoutSensorAccelerometer.setTextSensorType("Accelerometer");
        mLayoutSensorAccelerometer.setVisibility(View.VISIBLE);

        mSensorsHandler.addAccelerometer();
        if (mSensorsHandler.accelerometerAvailable())
        {
            mAccelerometer = true;
            mLayoutSensorAccelerometer.setSensorAvailable(true);

//// remove 2 - begin
/*
            mRadioAccelerometerDelay = mLayoutSensorAccelerometer.getRadioGroup();
            mRadioAccelerometerDelay.setOnCheckedChangeListener(new OnCheckedChangeListener()
            {
                public void onCheckedChanged(RadioGroup group, int checkedId)
                {
                    Log.d(tag, "SensorsActivity - ACCELEROMETER - onCheckedChanged : " + checkedId);
                    switch(checkedId)
                    {
                    case 0:
                        mSensorsHandler.setAccelerometerDelay(SensorsHandler.DELAY_NORMAL);
                        break;
                    case 1:
                        mSensorsHandler.setAccelerometerDelay(SensorsHandler.DELAY_GAME);
                        break;
                    case 2:
                        mSensorsHandler.setAccelerometerDelay(SensorsHandler.DELAY_FASTEST);
                        break;
                    }
                }
            });
*/
//// remove 2 - end
        }
    }

    // Orientation
    private void setOrientationView()
    {
        mLayoutSensorOrientation.setTextSensorType("Orientation");
        mLayoutSensorOrientation.setVisibility(View.VISIBLE);

        mSensorsHandler.addOrientation();
        if (mSensorsHandler.orientationAvailable())
        {
          	mOrientation = true;
            mLayoutSensorOrientation.setSensorAvailable(true);

//// remove 2 - begin
/*
            mRadioOrientationDelay = mLayoutSensorOrientation.getRadioGroup();
            mRadioOrientationDelay.setOnCheckedChangeListener(new OnCheckedChangeListener()
            {
                public void onCheckedChanged(RadioGroup group, int checkedId)
                {
                    Log.d(tag, "SensorsActivity - ORIENTATION - onCheckedChanged : " + checkedId);
                    switch(checkedId)
                    {
                    case 0:
                        mSensorsHandler.setOrientationDelay(SensorsHandler.DELAY_NORMAL);
                        break;
                    case 1:
                        mSensorsHandler.setOrientationDelay(SensorsHandler.DELAY_GAME);
                        break;
                    case 2:
                        mSensorsHandler.setOrientationDelay(SensorsHandler.DELAY_FASTEST);
                        break;
                    }
                }
            });
*/
//// remove 2 - end
        }
    }


    public void updateAccelerometer(float v0, float v1, float v2)
    {
// !!!! TODO : shouldn't be necessary, else wouldn't be here ... !!!!
        if (mAccelerometer)
        {
//?            synchronized (this)
//?            {
                mLayoutSensorAccelerometer.updateSensorValues(v0, v1, v2);
//?            }
        }
    }

    public void updateOrientation(float v0, float v1, float v2)
    {
// !!!! TODO : shouldn't be necessary, else wouldn't be here ... !!!!
        if (mOrientation)
        {
//?            synchronized (this)
//?            {
                mLayoutSensorOrientation.updateSensorValues(v0, v1, v2);
//?            }
        }
    }


    @Override
    protected void onResume()
    {
        super.onResume();

        mSensorsHandler.registerListeners();
    }

    @Override
    protected void onPause()
    {
        super.onPause();

        mSensorsHandler.unregisterListeners();
    }

    @Override
    protected void onDestroy()
    {
        super.onDestroy();

        mSensorsHandler.release();
    }

}
