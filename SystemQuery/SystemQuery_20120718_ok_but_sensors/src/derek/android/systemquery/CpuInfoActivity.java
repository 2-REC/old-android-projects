package derek.android.systemquery;

import java.io.IOException;
import java.io.InputStream;

import android.app.Activity;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

public class CpuInfoActivity extends Activity
{
    private TextView mTextCpuInfo;


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

// !!!! TODO : should have a title ... !!!!
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        setContentView(R.layout.cpu_info);

        mTextCpuInfo = (TextView) findViewById(R.id.text_cpu_info);

        mTextCpuInfo.setText(ReadCpuInfo());
    }

    private String ReadCpuInfo()
    {
        ProcessBuilder cmd;
        String result="";
        try
        {
            String[] args = {"/system/bin/cat", "/proc/cpuinfo"};
            cmd = new ProcessBuilder(args);
            Process process = cmd.start();
            InputStream in = process.getInputStream();
            byte[] re = new byte[1024];
            while(in.read(re) != -1)
            {
//                System.out.println(new String(re));
                result = result + new String(re);
            }
            in.close();
        }
        catch(IOException ex)
        {
            ex.printStackTrace();
        }
        return result;
    }

}
