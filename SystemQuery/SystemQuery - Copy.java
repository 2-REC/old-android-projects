package derek.android.query;

import android.app.Activity;
import android.os.Bundle;
import android.hardware.Sensor;
import android.hardware.SensorManager;


public class SystemQuery extends Activity
{
    // Build
    private TextView mTextBuildVersionSDKINT;
    private TextView mTextBuildVersionCodeName;
    private TextView mTextBuildVersionIncremental;
    private TextView mTextBuildVersionRelease;
    private TextView mTextBuildBrand;
    private TextView mTextBuildCPUABI;
    private TextView mTextBuildCPUABI2;
    private TextView mTextBuildDevice;
    private TextView mTextBuildModel;
    private TextView mTextBuildManufacturer;
    private TextView mTextBuildProduct;
    private TextView mTextBuildUser;

    // Screen & Display
    private TextView mTextScreenDensity;
    private TextView mTextScreenDensityDPI;
    private TextView mTextScreenDensityScaled;
    private TextView mTextScreenResolutionW;
    private TextView mTextScreenResolutionH;
    private TextView mTextScreenXDPI;
    private TextView mTextScreenYDPI;

    // Sensors
// !!!! TODO : give more info about sensors !!!!
// (separate types, names, etc.)
    private TextView mTextSensorsPresent;
    private TextView mTextSensorsNb;
    private TextView mTextSensorOrientation;
    private SensorManager mSensorManager;
    List<Sensor> mSensorsList;

    // OpenGL
// !!!! TODO : get more info (capabilities, ...) !!!!
    private View mTextOpenGL;

/*
    // GPS
    private View mTextGPSLong;
    private View mTextGPSLat;
*/


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        setContentView(R.layout.system_query);

        // Build
        mTextBuildVersionSDKINT = (TextView) findViewById(R.id.text_build_ver_sdkint);
        mTextBuildVersionCodeName = (TextView) findViewById(R.id.text_build_ver_codename);
        mTextBuildVersionIncremental = (TextView) findViewById(R.id.text_build_ver_inc);
        mTextBuildVersionRelease = (TextView) findViewById(R.id.text_build_ver_release);
        mTextBuildModel = (TextView) findViewById(R.id.text_build_model);
        mTextBuildManufacturer = (TextView) findViewById(R.id.text_build_manuf);
        mTextBuildProduct = (TextView) findViewById(R.id.text_build_product);
        mTextBuildBrand = (TextView) findViewById(R.id.text_build_brand);
        mTextBuildCPUABI = (TextView) findViewById(R.id.text_build_cpuabi1);
        mTextBuildCPUABI2 = (TextView) findViewById(R.id.text_build_cpuabi2);
        mTextBuildDevice = (TextView) findViewById(R.id.text_build_device);
        mTextBuildUser = (TextView) findViewById(R.id.text_build_user);

        mTextBuildVersionSDKINT.setText(String.valueOf(android.os.Build.VERSION.SDK_INT));
        mTextBuildVersionCodeName.setText(android.os.Build.VERSION.CODENAME);
        mTextBuildVersionIncremental.setText(android.os.Build.VERSION.INCREMENTAL);
        mTextBuildVersionRelease.setText(android.os.Build.VERSION.RELEASE);
        mTextBuildModel.setText(android.os.Build.MODEL);
        mTextBuildManufacturer.setText(android.os.Build.MANUFACTURER);
        mTextBuildProduct.setText(android.os.Build.PRODUCT);
        mTextBuildBrand.setText(android.os.Build.BRAND);
        mTextBuildCPUABI.setText(android.os.Build.CPU_ABI);
        mTextBuildCPUABI2.setText(android.os.Build.CPU_ABI2);
        mTextBuildDevice.setText(android.os.Build.DEVICE);
        mTextBuildUser.setText(android.os.Build.USER);


        // Screen & Display
        mTextScreenDensityDPI = (TextView) findViewById(R.id.text_screen_density_dpi);
        mTextScreenDensity = (TextView) findViewById(R.id.text_screen_density);
        mTextScreenDensityScaled = (TextView) findViewById(R.id.text_screen_density_scaled);
        mTextScreenResolutionW = (TextView) findViewById(R.id.text_screen_resolution_width);
        mTextScreenResolutionH = (TextView) findViewById(R.id.text_screen_resolution_height);
        mTextScreenXDPI = (TextView) findViewById(R.id.text_screen_density_xdpi);
        mTextScreenYDPI = (TextView) findViewById(R.id.text_screen_density_ydpi);

        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);

// !!!! TODO : make more "readable" display !!!!
// (replace some values by texts, such as "DENSITY_MEDIUM", ...)
/*
  DENSITY_DEFAULT : 160
  DENSITY_LOW : 120
  DENSITY_MEDIUM : 160
  DENSITY_HIGH : 240
  DENSITY_XHIGH : 320
  DENSITY_TV : 213
*/
        mTextScreenDensityDPI.setText(String.valueOf(metrics.densityDpi));
        mTextScreenDensity.setText(String.valueOf(metrics.density));
        mTextScreenDensityScaled.setText(String.valueOf(metrics.scaledDensity));
        mTextScreenResolutionW.setText(String.valueOf(metrics.widthPixels));
        mTextScreenResolutionH.setText(String.valueOf(metrics.heightPixels));
        mTextScreenXDPI.setText(String.valueOf(metrics.xdpi));
        mTextScreenYDPI.setText(String.valueOf(metrics.ydpi));


        // Sensors
        mTextSensorsPresent = (TextView) findViewById(R.id.text_sensors_presents);
        mTextSensorsNb = (TextView) findViewById(R.id.text_sensors_nb);
        mTextSensorsNb.setVisibility(View.GONE);
        mTextSensorOrientation = (TextView) findViewById(R.id.text_sensor_orientation);
        mTextSensorOrientation.setVisibility(View.GONE);

        mSensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        if (mSensorManager != null)
        {
            mTextSensorsPresent.setText("YES");

            mSensorsList = mSensorManager.getSensorList(Sensor.TYPE_ALL);
            mTextSensorsNb.setVisibility(View.VISIBLE);
            if (mSensorsList == null)
                mTextSensorsNb.setText("0 - null");
            else
                mTextSensorsNb.setText(String.valueOf(mSensorsList.size()));

            mTextSensorOrientation.setVisibility(View.VISIBLE);
            Sensor orientation = mSensorManager.getDefaultSensor(Sensor.TYPE_ORIENTATION);
            if (orientation != null)
            {
                mTextSensorOrientation.setVisibility(View.VISIBLE);
                mTextSensorOrientation.setText("available");
            }
        }
//?        else
//?            ...


        // OpenGL
        mTextOpenGL = (TextView) findViewById(R.id.text_opengl_version);
        ActivityManager am = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        ConfigurationInfo info = am.getDeviceConfigurationInfo();
        mTextOpenGL.setText(String.valueOf(info.reqGlEsVersion));


/*
        // GPS
        LocationManager locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);

        LocationListener locationListener = new LocationListener()
        {
            public void onLocationChanged(Location location)
            {
                updateLocation(location);
            }
            public void onStatusChanged(String provider, int status, Bundle extras) {}
            public void onProviderEnabled(String provider) {}
            public void onProviderDisabled(String provider) {}
        };
// !!!! TODO : change timing parameters to be less battery consuming !!!!
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locationListener);
*/

    }


/*
    // GPS
    private void updateLocation (Location location)
    {
        ...

// !!!! TODO : should maybe be done elsewhere ... !!!!
// (here we will only get 1 position update - is it enough ?)
        locationManager.removeUpdates(locationListener);
    }
*/


/*
!!!! TODO : display orientation sensor info !!!!
         sm = (SensorManager) getSystemService(SENSOR_SERVICE);
         setContentView(R.layout.main);
         xViewA = (TextView) findViewById(R.id.xbox);
         yViewA = (TextView) findViewById(R.id.ybox);
         zViewA = (TextView) findViewById(R.id.zbox);
         xViewO = (TextView) findViewById(R.id.xboxo);
         yViewO = (TextView) findViewById(R.id.yboxo);
         zViewO = (TextView) findViewById(R.id.zboxo);
    }
public void onSensorChanged(int sensor, float[] values) {
synchronized (this) {
Log.d(tag, "onSensorChanged: " + sensor + ", x: " + values[0] + ", y: " + values[1] + ", z: " + values[2]);
if (sensor == SensorManager.SENSOR_ORIENTATION) {
xViewO.setText("Orientation X: " + values[0]);
yViewO.setText("Orientation Y: " + values[1]);
zViewO.setText("Orientation Z: " + values[2]);
}
if (sensor == SensorManager.SENSOR_ACCELEROMETER) {
xViewA.setText("Accel X: " + values[0]);
yViewA.setText("Accel Y: " + values[1]);
zViewA.setText("Accel Z: " + values[2]);
}
}
}
public void onAccuracyChanged(int sensor, int accuracy) {
Log.d(tag,"onAccuracyChanged: " + sensor + ", accuracy: " + accuracy);
}
@Override
protected void onResume() {
super.onResume();
// register this class as a listener for the orientation and accelerometer sensors
sm.registerListener(this,
SensorManager.SENSOR_ORIENTATION |SensorManager.SENSOR_ACCELEROMETER,
SensorManager.SENSOR_DELAY_NORMAL);
}


*/


}
