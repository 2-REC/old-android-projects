


(- could separate the sensors ...)

- check that titles are correctly set for each screen

- check that buttons resize properly on each device
  (and that styles are ok)

--------

- add a "CPU Info" screen
  - where: "CPU Info" button in "Build/System" screen
  - role: displays the cpu info string
  - how: display content of "/proc/cpuinfo" in a scrollable TextView


- add a "Sizes Display" screen
  - where: "Sizes Display" button in "Display" screen (or on Main screen ?)
  - role: displays the different measures sizes
  - how: use code of "measures_tester.xml"
    (could use a slider/text input to control the size of the lines ...)


????
- add a kind of DPI calculator (in "Display" screen)
  (enter a dpi or px, then get inch, etc.)
????


- add memory/storage info (total, available, etc.)
  - SD
  - RAM
  - flash
  - etc. ?

  => basically the RAM size of the device :
    getMemoryInfo (ActivityManager.MemoryInfo)
    => public long totalMem (availMem ?)

????
OR:
MemoryInfo mi = new MemoryInfo(); 
ActivityManager activityManager = (ActivityManager) getSystemService(ACTIVITY_SERVICE); 
activityManager.getMemoryInfo(mi); 
long availableMegs = mi.availMem / 1048576L; 
????


- add a "Cameras" screen
  - list of available cameras (front, back, etc.?)
  - main/basic info on camera (resolution, ...)
  (- full detailed info on camera (settings) )
  - advanced settings/options (face detection, etc.)


- add an "Audio/Video" screen
  - available audio codecs
  - available video codecs



- open gl ...
  (ask for parameters, and create a small preview window ?)


- find a way to save all the queried data ...
 (put everything in same activity, different views ; or use intent & "return from activity" )



--------







- see how to use custom views and instantiate them
(=> rewrite the whole app in 1 activity, creating required views as needed)
=> problems with inflations, see how to do - use "LAyoutInflater.Inflate" ?





- Install APK on Emulator (if only one) :
  - run emulator
  - copy apk to emulator :
	cd "Program Files\android-sdk-...\platform-tools"
	adb install -e <path_to_app>\<app_name>.apk

- Install APK on Device (if only one) :
  - connect device
  - copy apk to device :
	cd "Program Files\android-sdk-...\platform-tools"
	adb install -d <path_to_app>\<app_name>.apk


- Directing Commands to a Specific Emulator/Device Instance :
	adb -s <serialNumber> <command>
	(ie: adb -s emulator-5556 install helloWorld.apk)


- List of connected emulators/devices :
	adb devices
  => returns list of [serialNumber] [state]
	- serialNumber : <type>-<consolePort> (ie: emulator-5554)
	- state        : offline | device





========================


info :
------


- Optimize UI for different screen sizes and densities:
  => The size buckets are handset (smaller than 600dp) and tablet (larger than or equal 600dp).
     The density buckets are LDPI, MDPI, HDPI, and XHDPI.
  - designg alternative layouts for some of the different size buckets,
  - provide alternative bitmap images for different density buckets.


  - not hard to figure out a minimum usable size in dip; everything is scaled so 160dip = 1"

  - 48-50 dip is the smallest you should go for touchable objects (9-10mm)

  - Use dp for anything but for fonts. For fonts use sp

  - Never use inch unless doing something like putting a ruler on the screen
    (that really has to be a certain size in inches)
    => always use dp and sp unless you know why you are using a different unit of size.



- Minimum expected resolutions:
  - xlarge screens are at least 960dp x 720dp
  - large screens are at least 640dp x 480dp
  - normal screens are at least 470dp x 320dp
  - small screens are at least 426dp x 320dp


- px : Pixels
  => corresponds to actual pixels on the screen. 


- in : Inches
  => based on the physical size of the screen. 


- mm : Millimeters
  => based on the physical size of the screen. 


- pt : Points
  => 1/72 of an inch based on the physical size of the screen. 


- dp : Density-independent Pixels
  => abstract unit based on physical density of the screen.
     (relative to a 160 dpi screen, so 1dp = 1px on a 160 dpi screen)

     The ratio of dp-to-pixel will change with the screen density, but not necessarily in direct proportion.
     Note: The compiler accepts both "dip" and "dp", though "dp" is more consistent with "sp". 


- sp : Scale-independent Pixels
  => this is like the dp unit, but it is also scaled by the user's font size preference.
     It is recommended to use this unit when specifying font sizes,
     so they will be adjusted for both the screen density and user's preferences.



