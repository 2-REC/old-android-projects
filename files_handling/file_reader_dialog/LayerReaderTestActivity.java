package derek.android.testapp;

import android.app.Activity;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;


public class FileReaderActivity extends Activity
{
    private DialogExplorer dialogExplorer;

    private Button mButtonOpenExplorer;
    private Button mButtonParseFile;

    private TextView mTextFile;
    private TextView mTextInfo;


    @Override
    public void onCreate( Bundle savedInstanceState )
    {
        super.onCreate( savedInstanceState );

        requestWindowFeature( Window.FEATURE_NO_TITLE );
        getWindow().addFlags( WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON );

        setContentView( R.layout.file_reader );

        mTextFile = (TextView) findViewById( R.id.text_file );
        mTextInfo = (TextView) findViewById( R.id.text_fileinfo );

        dialogExplorer = new DialogExplorer( this, mTextFile );

        mButtonOpenExplorer = (Button)findViewById( R.id.button_opendialog );
        mButtonOpenExplorer.setOnClickListener( dialogExplorer.getOpenListener() );

        mButtonParseFile = (Button)findViewById( R.id.button_parsefile );
        mButtonParseFile.setOnClickListener(
            new OnClickListener()
            {
                @Override
                public void onClick( View view )
                {
                    ParseFile( mTextFile.getText() );
                }
            }
        );
    }

    // this method could be abstract, or called at the end of overridden method
    private void ParseFile( String filename )
    {
        mTextInfo.setText( filename );
    }

}
