package derek.android.testapp;

package com.AndroidCustomDialog;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.os.Environment;
import android.app.Dialog;
import android.view.View;
import android.view.View.OnClickListener;

import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;


public class DialogExplorer
{
    private static final int DIALOG_EXPLORER_ID = 0;

    private File mRootFolder;
    private File mCurrentFolder;

    private View.OnClickListener mButtonOpenDialogListener;
    private Activity mParent;

    private Button mButtonUp;
    private TextView mCurrentFolderText;
    private ListView mFilesListView;

    private List<String> mFileList = new ArrayList<String>();

    private TextView mReturnTextView;


    public DialogExplorer( Activity parent, TextView textview )
    {
        mParent = parent;
        mReturnTextView = textview;

        mRootFolder = new File( Environment.getExternalStorageDirectory().getAbsolutePath() );
        mCurrentFolder = mRootFolder;

        mButtonOpenDialogListener =
            new View.OnClickListener()
            {
                @Override
                public void onClick( View view )
                {
                    showDialog( DIALOG_EXPLORER_ID );
                }
            };
    }


    @Override
    protected Dialog onCreateDialog( int id )
    {
        Dialog dialog = null;

        switch( id )
        {
        case DIALOG_EXPLORER_ID:
            dialog = new Dialog( parent );
            dialog.setContentView( R.layout.dialog_explorer_layout );
            dialog.setTitle( "File Explorer" );
            dialog.setCancelable( true );
            dialog.setCanceledOnTouchOutside( true );

            mCurrentFolderText = (TextView)dialog.findViewById( R.id.text_currentfolder );

            mButtonUp = (Button)dialog.findViewById( R.id.button_up );
            mButtonUp.setOnClickListener(
                new OnClickListener()
                {
                    @Override
                    public void onClick( View view )
                    {
                        ListDir( mCurrentFolder.getParentFile() );
                    }
                }
            );

            mFilesListView = (ListView)dialog.findViewById( R.id.list_files );
            mFilesListView.setOnItemClickListener(
                new OnItemClickListener()
                {
                    @Override
                    public void onItemClick( AdapterView<?> parent, View view, int position, long id )
                    {
                        File selected = new File( fileList.get( position ) );
                        if ( selected.isDirectory() )
                        {
                            ListDir( selected );
                        }
                        else
                        {
                            mReturnTextView.setText( selected.toString() );
                            dismissDialog( DIALOG_EXPLORER_ID );
                        }
                    }
                }
            );
            break;
        }
        return dialog;
    }

    private void ListDir( File f )
    {
        if ( f.equals( mRootFolder ) )
        {
            mButtonUp.setEnabled( false );
        }
        else
        {
            mButtonUp.setEnabled( true );
        }
        mCurrentFolder = f;
        mCurrentFolderText.setText( f.getPath() );

        File[] files = f.listFiles();
        fileList.clear();
        for ( File file : files )
        {
            fileList.add( file.getPath() );
        }

        ArrayAdapter<String> directoryList =
            new ArrayAdapter<String>( this, android.R.layout.simple_list_item_1, fileList );
        mFilesListView.setAdapter( directoryList );
    }

    public View.OnClickListener getOpenListener()
    {
        return mButtonOpenDialogListener;
    }

}
