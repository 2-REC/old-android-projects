//? package derek.android.....;

import android.app.Activity;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;


public abstract class FileReaderActivity extends Activity
{
    private static final int PICK_FILE_REQUEST = 0;

    private String mFile;
    private String mFolder;

    private Button mButtonOpenExplorer;
    private Button mButtonParseFile;

    private TextView mTextFile;
    private TextView mTextContent;


    private View.OnClickListener mButtonOpenExplorerListener = new View.OnClickListener()
    {
        public void onClick( View view )
        {
// !!!! ???? TODO : difference if call with "this" instead of "getBaseContext()" ? ???? !!!!
//?            Intent intent = new Intent( this, FileExplorerActivity.class );
            Intent intent = new Intent( getBaseContext(), FileExplorerActivity.class );
// !!!! ???? TODO : needed ? ???? !!!!
//?            intent.putExtras( getIntent() );
// !!!! TODO : use another root path !!!!
// => either local path or something else ( given )
            intent.putExtra( "root_folder", "/" );
            startActivityForResult( intent, PICK_FILE_REQUEST );
        }
    };


    @Override
    public void onCreate( Bundle savedInstanceState )
    {
        super.onCreate( savedInstanceState );

        requestWindowFeature( Window.FEATURE_NO_TITLE );
        getWindow().addFlags( WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON );

        setContentView( R.layout.file_reader );

        mTextFile = (TextView) findViewById( R.id.text_file );
        mTextContent = (TextView) findViewById( R.id.text_filecontent );

        mButtonOpenExplorer = (Button)findViewById( R.id.button_openexplorer );
        mButtonOpenExplorer.setOnClickListener( mButtonOpenExplorerListener );

        mButtonParseFile = (Button)findViewById( R.id.button_parsefile );
        mButtonParseFile.setOnClickListener(
            new OnClickListener()
            {
                @Override
                public void onClick( View view )
                {
                    ParseFile();
                }
            }
        );
    }

    @Override
    protected void onActivityResult( int requestCode, int resultCode, Intent intent )
    {
// !!!! ???? TODO : needed ? ???? !!!!
        super.onActivityResult( requestCode, resultCode, intent );

        if ( requestCode == PICK_FILE_REQUEST )
        {
            if ( resultCode == RESULT_OK )
            {
// !!!! ???? TODO : which one is best ? ???? !!!!
//?                mFile = intent.getExtras().getString( "filename" );
//?                mFolder = intent.getExtras().getString( "filepath" );
                mFile = intent.getStringExtra( "filename" );
                mFolder = intent.getStringExtra( "filepath" );

                mTextFile.setText( mFile );
            }
// !!!! TODO : handle cancel ... !!!!
/*
            else if ( resultCode == RESULT_CANCEL )
            {
                mFile = "";
                mFolder = "";
//?                mTextFile.setText( "" );
//?                mTextContent.setText( "" );
            }
*/
        }

    }

    private abstract ParseFile();
}
