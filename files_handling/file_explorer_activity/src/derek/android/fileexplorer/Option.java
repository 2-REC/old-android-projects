//package derek.android.fileexplorer;

/*
TODO :
- change class name ( such as "FileEntry" )
- change fields : name, path, type, size, (extra data)
  => and propagate changes in other classes !!!!
*/

public class Option implements Comparable<Option>
{
    private String mName;
    private String mData;
    private String mPath;

    public Option( String name, String data, String path )
    {
        mName = name;
        mData = data;
        mPath = path;
    }

    @Override
    public int compareTo( Option option )
    {
// !!!! ???? TODO : shouldn't be like that ? ???? !!!!
//        if ( mName != null )
        if ( ( mName != null ) && ( option != null ) && ( option.getName() ) )
        {
            return mName.toLowerCase().compareTo( option.getName().toLowerCase() );
        }
        else
        {
            throw new IllegalArgumentException();
        }
    }

    public String getName()
    {
        return mName;
    }

    public String getData()
    {
        return mData;
    }

    public String getPath()
    {
        return mPath;
    }
}
