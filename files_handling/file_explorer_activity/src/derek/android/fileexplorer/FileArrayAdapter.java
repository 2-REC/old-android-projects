//package derek.android.fileexplorer;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;


public class FileArrayAdapter extends ArrayAdapter<Option>
{
    private Context mContext;
    private int mId;
    private List<Option> mItems;

    public FileArrayAdapter( Context context, int textViewResourceId, List<Option> objects )
    {
        super( context, textViewResourceId, objects );
        mContext = context;
        mId = textViewResourceId;
        mItems = objects;
    }

    public Option getItem( int i )
    {
        return mItems.get( i );
    }

    @Override
    public View getView( int position, View convertView, ViewGroup parent )
    {
        View view = convertView;
        if ( view == null )
        {
            LayoutInflater vi = (LayoutInflater)mContext.getSystemService( Context.LAYOUT_INFLATER_SERVICE );
            view = vi.inflate( mId, null );
        }

        final Option option = mItems.get( position );
        if ( option != null )
        {
            TextView text1 = (TextView) view.findViewById( R.id.TextView01 );
            TextView text2 = (TextView) view.findViewById( R.id.TextView02 );

            if ( text1 != null )
            {
                text1.setText( option.getName() );
            }
            if ( text2 != null )
            {
                text2.setText( option.getData() );
            }
        }
        return view;
   }
}
