//package derek.android.fileexplorer;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import android.app.ListActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;


public class FileExplorerActivity extends ListActivity
{
    private static final String ROOT_FOLDER = "sdcard";

    private String mRootFolder;
    private File mCurrentFolder;

    private FileArrayAdapter mAdapter;


    @Override
    public void onCreate( Bundle savedInstanceState )
    {
        super.onCreate( savedInstanceState );

// !!!! ???? TODO : OK ? ???? !!!!
// => can have no Intent ? (null)
        final Intent mCallingIntent = getIntent();
// !!!! ???? TODO : "getStringExtra" ? ???? !!!!
        mRootFolder = mCallingIntent.getStringExtra( "root_folder", ROOT_FOLDER );
//?        mRootFolder = mCallingIntent.getStringExtra( "root_folder", "/" + ROOT_FOLDER + "/" );
////

        mCurrentFolder = new File( "/" + mRootFolder + "/" );
//?        mCurrentFolder = new File( mRootFolder );
        fill( mCurrentFolder );

    }

    @Override
    protected void onListItemClick( ListView list, View view, int position, long id )
    {
        super.onListItemClick( list, view, position, id );

        Option option = mAdapter.getItem( position );
// !!!! TODO : should change "data" with "type" !!!!
// => could also have "data", but need the "type" ( folder or file )
        if ( option.getData().equalsIgnoreCase( "folder" )
            || option.getData().equalsIgnoreCase( "parent directory" ) )
        {
            mCurrentFolder = new File( option.getPath() );
            fill( mCurrentFolder );
        }
        else
        {
            onFileClick( option );
        }
    }

    private void fill( File file )
    {
        List<Option> listFolders = new ArrayList<Option>();
        List<Option> listFiles = new ArrayList<Option>();

        File[] files = file.listFiles();

// !!!! TODO : instead of setting title should set a text field !!!!
        this.setTitle( "Current Dir: " + file.getName() );

        try
        {
            for ( File ff: files )
            {
                if ( ff.isDirectory() )
                {
// !!!! TODO : could get rid of "data" field, or use something else !!!!
                    listFolders.add(
                        new Option( ff.getName(), "Folder", ff.getAbsolutePath() )
                    );
                }
                else
                {
                    listFiles.add(
                        new Option( ff.getName(), "File Size: " + ff.length(), ff.getAbsolutePath() )
                    );
                }
            }
        }
        catch( Exception e )
        {
// !!!! ???? TODO : do something ? ???? !!!!
        }

        Collections.sort( listFolders );
        Collections.sort( listFiles );

        listFolders.addAll( listFiles );

// !!!! ???? TODO : need limit to "rootFolder" ? ???? !!!!
        if ( !file.getName().equalsIgnoreCase( mRootFolder ) )
        {
            listFolders.add( 0, new Option( "..", "Parent Directory", file.getParent() ) );
        }

// !!!! ???? TODO : need "FileExplorerActivity." before "this" ? ???? !!!!
        mAdapter = new FileArrayAdapter( FileExplorerActivity.this, R.layout.file_view, listFolders );
        this.setListAdapter( mAdapter );
    }

    private void onFileClick( Option option )
    {
// !!!! TODO : may not be needed !!!!
// => but to be sure ...
        if ( getCallingActivity() != null )
        {
// !!!! ???? TODO : need to keep the caller Intent ? ???? !!!! 
//?        setResult( RESULT_OK, mCallingIntent );
            Intent intent = new Intent();
// !!!! ???? TODO : may not need to send as "extra data", but straight as "data" ? ???? !!!!
            intent.putExtra( "filename", option.getName() );
            intent.putExtra( "filepath", option.getPath() );
            setResult( RESULT_OK, intent );
        }
// !!!! ???? TODO : OK here ? ???? !!!!
        finish();
    }
}
