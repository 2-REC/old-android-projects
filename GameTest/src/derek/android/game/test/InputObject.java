package derek.android.game.test;

import java.util.concurrent.ArrayBlockingQueue;

import android.view.KeyEvent;
import android.view.MotionEvent;
//// SENSORS - begin
/*
// !!!! ???? TODO : SensorEvent - OK to handle as other inputs ???? !!!!
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorManager;
// SENSOR SIM
//import org.openintents.sensorsimulator.hardware.Sensor;
//import org.openintents.sensorsimulator.hardware.SensorEvent;
//import org.openintents.sensorsimulator.hardware.SensorManagerSimulator;
*/
//// SENSORS - end


public class InputObject
{
    public static final byte EVENT_TYPE_KEY = 1;
    public static final byte EVENT_TYPE_TOUCH = 2;
//// SENSORS - begin
/*
// !!!! ???? TODO : SensorEvent - OK to handle as other inputs ???? !!!!
    public static final byte EVENT_TYPE_SENSOR = 3;
*/
//// SENSORS - end

    public static final int ACTION_KEY_DOWN = 1;
    public static final int ACTION_KEY_UP = 2;

    public static final int ACTION_TOUCH_DOWN = 3;
    public static final int ACTION_TOUCH_MOVE = 4;
    public static final int ACTION_TOUCH_UP = 5;

//// SENSORS - begin
/*
// !!!! ???? TODO : SensorEvent - OK to handle as other inputs ???? !!!!
    public static final int ACTION_ORIENTATION = 6;
*/
//// SENSORS - end

    public ArrayBlockingQueue<InputObject> mObjectPool;
    public byte mEventType;
    public long mTime;
    public int mAction;
    public int mKeyCode;
    public int mX;
    public int mY;

    public InputObject(ArrayBlockingQueue<InputObject> objectPool)
    {
        mObjectPool = objectPool;
    }

    public void useEvent(KeyEvent event)
    {
        mEventType = EVENT_TYPE_KEY;

        int a = event.getAction();
        switch (a)
        {
        case KeyEvent.ACTION_DOWN:
            mAction = ACTION_KEY_DOWN;
            break;
        case KeyEvent.ACTION_UP:
            mAction = ACTION_KEY_UP;
            break;
        default:
            mAction = 0;
        }
        mTime = event.getEventTime();
        mKeyCode = event.getKeyCode();
    }

    public void useEvent(MotionEvent event)
    {
        mEventType = EVENT_TYPE_TOUCH;

        int a = event.getAction();
        switch (a)
        {
        case MotionEvent.ACTION_DOWN:
            mAction = ACTION_TOUCH_DOWN;
            break;
        case MotionEvent.ACTION_MOVE:
            mAction = ACTION_TOUCH_MOVE;
            break;
        case MotionEvent.ACTION_UP:
            mAction = ACTION_TOUCH_UP;
            break;
        default:
            mAction = 0;
        }
        mTime = event.getEventTime();
        mX = (int) event.getX();
        mY = (int) event.getY();
    }

//// SENSORS - begin
/*
// !!!! ???? TODO : SensorEvent - OK to handle as other inputs ???? !!!!
    public void useEvent(SensorEvent event)
    {
        mEventType = EVENT_TYPE_SENSOR;

// !!!! ???? TODO : TYPE_ORIENTATION is deprecated ... what to use ? ???? !!!!
        int a = event.sensor.getType();
// SENSOR SIM
//        int a = event.type;
        switch (a)
        {
//        case Sensor.TYPE_ORIENTATION:
        case Sensor.TYPE_ACCELEROMETER:
            mAction = ACTION_ORIENTATION;
            break;
        default:
            mAction = 0;
        }
//        mTime = event.getEventTime();
//        mX = (int) event.values[1];
//        mY = (int) event.values[2];
//?        mZ = event.values[0];

// !!!! ???? TODO : better to use a local variables ???? !!!!
// (instead of SensorManager.DATA_Y & SensorManager.GRAVITY_EARTH)
//        mX = (int)event.values[SensorManager.DATA_Y];
//        mY = (int)event.values[SensorManager.DATA_Z];
        mX = (int)event.values[1];
        mY = (int)event.values[2];
// !!!! TODO : should use getRotationMatrix & getOrientation !!!!
//...
    }
*/
//// SENSORS - end

    public void useEventHistory(MotionEvent event, int historyItem)
    {
        mEventType = EVENT_TYPE_TOUCH;
        mAction = ACTION_TOUCH_MOVE;
        mTime = event.getHistoricalEventTime(historyItem);
        mX = (int) event.getHistoricalX(historyItem);
        mY = (int) event.getHistoricalY(historyItem);
    }

    public void returnToPool()
    {
        mObjectPool.add(this);
    }
}
