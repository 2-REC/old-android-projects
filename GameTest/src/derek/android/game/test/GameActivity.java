package derek.android.game.test;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.KeyEvent;
import android.view.MotionEvent;
//// Sensors - begin
/*
import android.hardware.SensorEvent;
// SENSOR SIM
//import org.openintents.sensorsimulator.hardware.SensorEvent;
*/
//// Sensors - end
import android.view.Window;
import android.view.WindowManager;
import java.util.concurrent.ArrayBlockingQueue;

//// Logs - begin
import android.util.Log;
//// Logs - end


public class GameActivity extends Activity
{
//// Logs - begin
    private static final String LOG_INIT_TAG = "LOG_INIT";
//// Logs - end

    public static final int DIALOG_QUIT_GAME = 0;

// !!!! TODO : should use something else than a thread ... !!!!
// (such as an object holding a thread and an object manager as in replica island)
    private GameThread mGameThread = null;
    private GameView mGameView = null;

    private PauseMenuView mPauseMenu = null;
    private Handler mHandler = null;

// !!!! TODO - WAIT MSG : should have a "wait message" when level is loading !!!!
// (could have a toast message ?)
//?    private View mWaitMessage;

    private ArrayBlockingQueue<InputObject> mInputObjectPool = null;

//// Sensors - begin
/*
    private SensorsHandler mSensorsHandler;
*/
//// Sensors - end


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

// !!!! TODO : change layout name (to "game" ?) !!!!
        setContentView(R.layout.game_main);

        mGameView = (GameView) findViewById(R.id.game_view);
        mPauseMenu = (PauseMenuView) findViewById(R.id.pause_menu);

        mHandler = new Handler()
        {
            @Override
            public void handleMessage(Message m)
            {
                if (m.getData().getBoolean("pause"))
                {
                    mPauseMenu.activate();
                }
                else if (m.getData().getBoolean("unpause"))
                {
                    if (m.getData().getBoolean("pause_menu"))
                        mGameThread.unpause(false);
                    mPauseMenu.deactivate();
                }
                else if (m.getData().getBoolean("quit"))
                {
                    if (m.getData().getBoolean("pause_menu"))
                        mGameThread.quit(false);
                    mPauseMenu.activate();
                    showDialog(DIALOG_QUIT_GAME);
                }
            }
        };
        mPauseMenu.setHandler(mHandler);
        mPauseMenu.deactivate();

//?        mWaitMessage = findViewById(R.id.pleaseWaitMessage);

// !!!! ???? TODO - AUDIO : need that ???? !!!!
//?        setVolumeControlStream(AudioManager.STREAM_MUSIC);

        createInputObjectPool();

//// Sensors - begin
/*
        mSensorsHandler = new SensorsHandler(this);
        mSensorsHandler.addAccelerometer();
        mSensorsHandler.setAccelerometerDelay(SensorsHandler.DELAY_GAME);
*/
//// Sensors - end

        initGame(mGameView, null, savedInstanceState);
    }

    private void initGame(GameView gameView, GameThread gameThread, Bundle savedInstanceState)
    {
//// Logs - begin
        Log.i(LOG_INIT_TAG, "GameActivity - initGame");
//// Logs - end

        if (savedInstanceState == null)
        {
//// Logs - begin
            Log.i(LOG_INIT_TAG, "GameActivity - initGame -  (savedInstanceState == null)");
//// Logs - end

            // new game
            mGameThread = new GameThread(mGameView);
            mGameThread.setHandler(mHandler);
            mGameView.setThread(mGameThread);

// !!!! ???? TODO - TEST : OK to forward Intent ? ???? !!!!
            mGameThread.initGame(getIntent());
        }
        else
        {
//// Logs - begin
            Log.i(LOG_INIT_TAG, "GameActivity - initGame -  (savedInstanceState != null)");
//// Logs - end

            // resume game
            if (mGameThread != null)
            {
//// Logs - begin
                Log.i(LOG_INIT_TAG, "GameActivity - initGame -    (mGameThread != null)");
//// Logs - end

// !!!! ???? TODO - OK to set Thread.Handler & View.Thread, without releasing old ones ? ???? !!!!
                mGameThread.setHandler(mHandler);
                mGameView.setThread(mGameThread);

                mGameThread.restoreState(savedInstanceState);
            }
            else
            {
//// Logs - begin
                Log.i(LOG_INIT_TAG, "GameActivity - initGame -    (mGameThread == null)");
//// Logs - end

//!!!! ???? TODO : why like this ? utility of gameThread ? ???? !!!!
//// ORIG CODE - begin
/*
                gameThread = new GameThread(mGameView);
                mGameView.setThread(gameThread);
                mGameThread = mGameView.getThread();
                mGameThread.restoreState(savedInstanceState);
                mGameThread.setState(GameThread.STATE_READY);
*/
//// ORIG CODE - mid
//=> better like this no ?
                mGameThread = new GameThread(mGameView);
                mGameThread.setHandler(mHandler);
                mGameView.setThread(mGameThread);

                mGameThread.restoreState(savedInstanceState);
                //?gameThread = mGameThread;
//// ORIG CODE - end
            }
        }
    }

    @Override
    protected void onDestroy()
    {
        super.onDestroy();

//// Logs - begin
        Log.i(LOG_INIT_TAG, "GameActivity - onDestroy");
//// Logs - end

        mHandler = null;
        mPauseMenu = null;

        mGameView.cleanup();

        mGameThread = null;
        mGameView = null;

//!!!! ???? TODO : need this ???? !!!!
//?        removeDialog(DIALOG_QUIT_GAME);
    }

    @Override
    protected void onPause()
    {
        super.onPause();

//// Logs - begin
        Log.i(LOG_INIT_TAG, "GameActivity - onPause");
//// Logs - end

        mGameThread.pause();

//// Sensors - begin
/*
        mSensorsHandler.unregisterListeners();
*/
//// Sensors - end
    }

    @Override
    protected void onResume()
    {
        super.onResume();

//// Logs - begin
        Log.i(LOG_INIT_TAG, "GameActivity - onResume");
//// Logs - end

//!!!! TODO : use preferences ... !!!!
/*
        // preferences may have changed
        SharedPreferences prefs = getSharedPreferences(PreferenceConstants.PREFERENCE_NAME, MODE_PRIVATE);
        final boolean debugLogs = prefs.getBoolean(PreferenceConstants.PREFERENCE_ENABLE_DEBUG, false);
*/

//// Sensors - begin
/*
        mSensorsHandler.registerListeners();
*/
//// Sensors - end
    }

    @Override
    protected void onSaveInstanceState(Bundle outState)
    {
        super.onSaveInstanceState(outState);

//// Logs - begin
        Log.i(LOG_INIT_TAG, "GameActivity - onSaveInstanceState");
//// Logs - end

        if(mGameThread != null)
        {
            mGameThread.saveState(outState);
        }
    }

    @Override
    public boolean onTouchEvent (MotionEvent event)
    {
        try
        {
            int hist = event.getHistorySize();
            if (hist > 0)
            {
                for (int i = 0; i < hist; i++)
                {
                    InputObject input = mInputObjectPool.take();
                    input.useEventHistory(event, i);
                    mGameThread.feedInput(input);
                }
            }
            InputObject input = mInputObjectPool.take();
            input.useEvent(event);
            mGameThread.feedInput(input);
        }
        catch (InterruptedException e)
        {
        }
// !!!! ???? TODO : keep that ???? !!!!
        // don't allow more than 60 motion events per second
        try
        {
           Thread.sleep(16);
        }
        catch (InterruptedException e)
        {
        }
        return true;
    }

    @Override
    public boolean onKeyDown (int keyCode, KeyEvent event)
    {
//!!!! ???? TO DO : make a special debug menu ... ???? !!!!
//        if (...)
//        {
//            if (keyCode == KeyEvent.KEYCODE_MENU)
//            {
//                if (mGameThread.isPaused())
//                {
//                    return false; // allow the menu to come up
//                }
//            }
//        }

        try
        {
            InputObject input = mInputObjectPool.take();
            input.useEvent(event);
            mGameThread.feedInput(input);
        }
        catch (InterruptedException e)
        {
        }
        return true;
    }

    @Override
    public boolean onKeyUp (int keyCode, KeyEvent event)
    {
        try
        {
            InputObject input = mInputObjectPool.take();
            input.useEvent(event);
            mGameThread.feedInput(input);
        }
        catch (InterruptedException e)
        {
        }
        return true;
    }

//// Sensors - begin
/*
    public void updateAccelerometer(SensorEvent event)
    {
// !!!! ???? TODO : SensorEvent - OK to handle as other inputs ???? !!!!
//////// begin
// !!!! ???? TODO : need to synchronise ???? !!!!
////?        synchronized (this)
////?        {
//            if (event.sensor.getType() == Sensor.TYPE_ORIENTATION)
//            {
//                final float x = event.values[1];
//                final float y = event.values[2];
//                final float z = event.values[0];
//                mGameThread.onOrientationEvent(x, y, z);
//            }
////?        }
//////// mid
        try
        {
//// Logs - begin
            Log.i(LOG_INIT_TAG, "updateAccelerometer");
            Log.i(LOG_INIT_TAG, "updateAccelerometer - 0 = " + String.valueOf(event.values[0]));
            Log.i(LOG_INIT_TAG, "updateAccelerometer - 1 = " + String.valueOf(event.values[1]));
            Log.i(LOG_INIT_TAG, "updateAccelerometer - 2 = " + String.valueOf(event.values[2]));
//// Logs - end

// !!!! TODO : should filter the events !!!!
// ???? DO THAT HERE ???? (or in THREAD, or in INPUTOBJECT ?)
// => if not interested in a direction, or value too small, etc. !!!!
//            if ( (event.values[1] > 1) || (event.values[1] < -1) )
//            {

            InputObject input = mInputObjectPool.take();
            input.useEvent(event);
            mGameThread.feedInput(input);
//            }
        }
        catch (InterruptedException e)
        {
        }
// !!!! ???? TODO : keep that ???? !!!!
        // don't allow more than 60 sensor events per second
        try
        {
           Thread.sleep(16);
        }
        catch (InterruptedException e)
        {
        }
//////// end
    }

    public void updateOrientation(SensorEvent event)
    {
    }
*/
//// Sensors - end


    private void createInputObjectPool()
    {
        mInputObjectPool = new ArrayBlockingQueue<InputObject>(GameThread.INPUT_QUEUE_SIZE);
        for (int i = 0; i < GameThread.INPUT_QUEUE_SIZE; i++)
        {
            mInputObjectPool.add(new InputObject(mInputObjectPool));
        }
    }


//!!!! ???? TODO : make a special debug menu ... ???? !!!!
/*
    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        super.onCreateOptionsMenu(menu);
        boolean handled = false;

        if (...)
        {
            menu.add(0, ..., 0, R.string.change_level);
            handled = true;
        }
        return handled;
    }
*/

//!!!! ???? TODO : make a special debug menu ... ???? !!!!
/*
    @Override
    public boolean onMenuItemSelected(int featureId, MenuItem item)
    {
        Intent i;
        switch(item.getItemId())
        {
        case ...:
            i = new Intent(this, some-other-Activity.class);
//?              i.putExtra("...", some-other-Activity.SOME_PARAM);
//?              startActivityForResult(i, ACTIVITY_...);
            startActivity(i);
            return true;
//!!!! TODO : would be useful to use method tracing !!!!
//        case METHOD_TRACING_ID:
//            if (mMethodTracing)
//            {
//                Debug.stopMethodTracing();
//            }
//            else
//            {
//                Debug.startMethodTracing("...");
//            }
//            mMethodTracing = !mMethodTracing;
//            return true;
        }
        return super.onMenuItemSelected(featureId, item);
    }
*/

//!!!! TODO : if have some startActivityForResult !!!!
/*
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent)
    {
        super.onActivityResult(requestCode, resultCode, intent);

        if (requestCode == ACTIVITY_...)
        {
            if (resultCode == RESULT_OK)
            {
                ...
            }
        }
        else if (requestCode == ACTIVITY_...)
        {
            ...
        }
    }
*/

//!!!! TODO : handle game events ... !!!!
//=> move that to game thread !!!!
/*
  public void onGameFlowEvent(int eventCode, int index)
  {
      switch (eventCode)
      {
          case GameFlowEvent.EVENT_END_GAME: 
              mGame.stop();
              finish();
              break;
          case GameFlowEvent.EVENT_RESTART_LEVEL:
//?                if (LevelTree.get(mLevelRow, mLevelIndex).restartable)
//?                {
//                  ...
                  mGame.restartLevel();
                  break;
//?                }
              // else, fall through and go to the next level.
          case GameFlowEvent.EVENT_GO_TO_NEXT_LEVEL:
//?                LevelTree.get(mLevelRow, mLevelIndex).completed = true;
//!!!! TODO : look at the tree level stuff to handle 'groups' of levels ... !!!!

//              if (mLevelRow < LevelTree.levels.size())
//              {
//?                    if (currentLevel.inThePast || LevelTree.levels.get(mLevelRow).levels.size() > 1)
//?                    {
                      // go to the level select.
                      Intent i = new Intent(this, LevelSelectActivity.class);
                      startActivityForResult(i, ACTIVITY_CHANGE_LEVELS);
//                      if (UIConstants.mOverridePendingTransition != null)
//                      {
//                         ...
//                      }
//?                    }
//?                    else
//?                    {
                      // go directly to the next level
                      mGame.setPendingLevel(currentLevel);
                      if (currentLevel.showWaitMessage) {
                          showWaitMessage();
                      }
                      else
                      {
                          hideWaitMessage();
                      }
                      mGame.requestNewLevel();
                  }
                  saveGame();

              }
             else
             {
                 // We beat the game!
          	   mLevelRow = 0;
          	   mLevelIndex = 0;
          	   mLastEnding = mGame.getLastEnding();
          	   mExtrasUnlocked = true;
          	   saveGame();
                 mGame.stop();
                 Intent i = new Intent(this, GameOverActivity.class);
                 startActivity(i);
                 if (UIConstants.mOverridePendingTransition != null) {
                 ...
  	            }
                 finish();
                 
             }
             break;
         case GameFlowEvent.EVENT_SHOW_ANIMATION:
      	   i = new Intent(this, AnimationPlayerActivity.class);
             i.putExtra("animation", index);
             startActivityForResult(i, ACTIVITY_ANIMATION_PLAYER);
             if (UIConstants.mOverridePendingTransition != null) {
	 		       try {
	 		    	  UIConstants.mOverridePendingTransition.invoke(AndouKun.this, R.anim.activity_fade_in, R.anim.activity_fade_out);
	 		       } catch (InvocationTargetException ite) {
	 		           DebugLog.d("Activity Transition", "Invocation Target Exception");
	 		       } catch (IllegalAccessException ie) {
	 		    	   DebugLog.d("Activity Transition", "Illegal Access Exception");
	 		       }
	            }
             break;
      	   
     }
  }
*/

// !!!! TODO - WAIT MSG : should have a "wait message" when level is loading !!!!
/*
  protected void showWaitMessage()
  {
      if (mWaitMessage != null)
      {
          mWaitMessage.setVisibility(View.VISIBLE);
          mWaitMessage.startAnimation(mWaitFadeAnimation);
      }
  }
  protected void hideWaitMessage()
  {
      if (mWaitMessage != null)
      {
          mWaitMessage.setVisibility(View.GONE);
          mWaitMessage.clearAnimation();
      }
  }
*/


    @Override
    protected Dialog onCreateDialog(int id)
    {
        Dialog dialog = null;
        if (id == DIALOG_QUIT_GAME)
        {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle(R.string.quit_dialog_title)
                   .setCancelable(false)
                   .setPositiveButton(R.string.quit_dialog_ok,
                                      new DialogInterface.OnClickListener()
                                      {
                                          public void onClick(DialogInterface dialog, int id)
                                          {
                                              finish();
//!!!! ???? TODO : want to handle activity transitions ???? !!!!
/*
                                              if (UIConstants.mOverridePendingTransition != null)
                                              {
                                                  ...
                                              }
*/
                                          }
                                      })
                   .setNegativeButton(R.string.quit_dialog_cancel,
                                      new DialogInterface.OnClickListener()
                                      {
                                          public void onClick(DialogInterface dialog, int id)
                                          {
                                              dialog.cancel();
                                          }
                                      })
                   .setMessage(R.string.quit_dialog_message);
            dialog = builder.create();
        }
        return dialog;
    }

//// INIT - begin
    public void setThread(GameThread newThread)
    {
        mGameThread = newThread;
    }
//// INIT - end

}
