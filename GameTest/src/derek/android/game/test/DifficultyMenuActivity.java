package derek.android.game.test;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

public class DifficultyMenuActivity extends Activity
{
    private View mBackground;

    private View mButtonEasy;
    private View mButtonMedium;
    private View mButtonHard;

// !!!! ???? TODO : are these useful ? why/where ? ???? !!!!
/*
    private View mTextEasy;
    private View mTextMedium;
    private View mTextHard;
*/

    private Animation mAnimationButton;
    private Animation mAnimationFadeOutBkg;
    private Animation mAnimationFadeOut;

    private boolean mJustCreated;


    private View.OnClickListener mButtonEasyListener = new View.OnClickListener()
    {
        public void onClick(View view)
        {
// !!!! TODO : change 'Game' class ... !!!!
            Intent intent = new Intent(getBaseContext(), GameActivity.class);
            intent.putExtras(getIntent());
            intent.putExtra("difficulty", 0);

            view.startAnimation(mAnimationButton);
            mAnimationFadeOutBkg.setAnimationListener(new StartActivityAfterAnimation(intent));
            mBackground.startAnimation(mAnimationFadeOutBkg);
            mButtonMedium.startAnimation(mAnimationFadeOut);
            mButtonHard.startAnimation(mAnimationFadeOut);

// !!!! ???? TODO : are these useful ? why/where ? ???? !!!!
//?            mTextEasy.startAnimation(mAnimationFadeOut);
//?            mTextMedium.startAnimation(mAnimationFadeOut);
//?            mTextHard.startAnimation(mAnimationFadeOut);
        }
    };

    private View.OnClickListener mButtonMediumListener = new View.OnClickListener()
    {
        public void onClick(View view)
        {
// !!!! TODO : change 'Game' class ... !!!!
            Intent intent = new Intent(getBaseContext(), GameActivity.class);
            intent.putExtras(getIntent());
            intent.putExtra("difficulty", 1);

            view.startAnimation(mAnimationButton);
            mAnimationFadeOutBkg.setAnimationListener(new StartActivityAfterAnimation(intent));
            mBackground.startAnimation(mAnimationFadeOutBkg);
            mButtonEasy.startAnimation(mAnimationFadeOut);
            mButtonHard.startAnimation(mAnimationFadeOut);

// !!!! ???? TODO : are these useful ? why/where ? ???? !!!!
//?            mTextEasy.startAnimation(mAnimationFadeOut);
//?            mTextMedium.startAnimation(mAnimationFadeOut);
//?            mTextHard.startAnimation(mAnimationFadeOut);
        }
    };

    private View.OnClickListener mButtonHardListener = new View.OnClickListener()
    {
        public void onClick(View view)
        {
// !!!! TODO : change 'Game' class ... !!!!
            Intent intent = new Intent(getBaseContext(), GameActivity.class);
            intent.putExtras(getIntent());
            intent.putExtra("difficulty", 2);

            view.startAnimation(mAnimationButton);
            mAnimationFadeOutBkg.setAnimationListener(new StartActivityAfterAnimation(intent));
            mBackground.startAnimation(mAnimationFadeOutBkg);
            mButtonEasy.startAnimation(mAnimationFadeOut);
            mButtonMedium.startAnimation(mAnimationFadeOut);

// !!!! ???? TODO : are these useful ? why/where ? ???? !!!!
//?            mTextEasy.startAnimation(mAnimationFadeOut);
//?            mTextMedium.startAnimation(mAnimationFadeOut);
//?            mTextHard.startAnimation(mAnimationFadeOut);
        }
    };

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.menu_difficulty);

        mBackground = findViewById(R.id.background);

        mButtonEasy = findViewById(R.id.button_easy);
        mButtonMedium = findViewById(R.id.button_medium);
        mButtonHard = findViewById(R.id.button_hard);

// !!!! ???? TODO : are these useful ? why/where ? ???? !!!!
/*
        mTextEasy = findViewById(R.id.text_easy);
        mTextMedium = findViewById(R.id.text_medium);
        mTextHard = findViewById(R.id.text_hard);
*/

        mButtonEasy.setOnClickListener(mButtonEasyListener);
        mButtonMedium.setOnClickListener(mButtonMediumListener);
        mButtonHard.setOnClickListener(mButtonHardListener);

        mAnimationButton = AnimationUtils.loadAnimation(this, R.anim.button_flicker);
        mAnimationFadeOutBkg = AnimationUtils.loadAnimation(this, R.anim.fade_out);
        mAnimationFadeOut = AnimationUtils.loadAnimation(this, R.anim.fade_out);        

        mJustCreated = true;

// !!!! ???? TODO : need that ???? !!!!
//?        setVolumeControlStream(AudioManager.STREAM_MUSIC);

    }

    @Override
    protected void onResume()
    {
        super.onResume();

        mAnimationButton.setAnimationListener(null);

        if (mBackground != null) {
        	mBackground.clearAnimation();
        }

        if (mJustCreated)
        {
            if (mButtonEasy != null)
            {
                mButtonEasy.startAnimation(AnimationUtils.loadAnimation(this, R.anim.button_slide));
            }
            if (mButtonMedium != null)
            {
                Animation anim = AnimationUtils.loadAnimation(this, R.anim.button_slide);
                anim.setStartOffset(500L);
                mButtonMedium.startAnimation(anim);
            }
            if (mButtonHard != null)
            {
                Animation anim = AnimationUtils.loadAnimation(this, R.anim.button_slide);
                anim.setStartOffset(1000L);
                mButtonHard.startAnimation(anim);
            }
            mJustCreated = false;
        }
        else
        {
            mButtonEasy.clearAnimation();
            mButtonMedium.clearAnimation();
            mButtonHard.clearAnimation();
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)
    {
        boolean result = true;
        if (keyCode == KeyEvent.KEYCODE_BACK)
        {
// !!!! ???? TODO : OK like this ???? !!!!
// !!!! ???? TODO : check if ok to pass null to StartActivityAfterAnimation ???? !!!!
            mAnimationFadeOutBkg.setAnimationListener(new StartActivityAfterAnimation(null));
            mBackground.startAnimation(mAnimationFadeOutBkg);
            mButtonEasy.startAnimation(mAnimationFadeOut);
            mButtonMedium.startAnimation(mAnimationFadeOut);
            mButtonHard.startAnimation(mAnimationFadeOut);

// !!!! ???? TODO : want to handle activity transitions ???? !!!!
/*
            if (UIConstants.mOverridePendingTransition != null)
            {
                try
                {
                    UIConstants.mOverridePendingTransition.invoke(DifficultyMenuActivity.this, R.anim.activity_fade_in, R.anim.activity_fade_out);
                }
                catch (InvocationTargetException ite)
                {
                    DebugLog.d("Activity Transition", "Invocation Target Exception");
                }
                catch (IllegalAccessException ie)
                {
                    DebugLog.d("Activity Transition", "Illegal Access Exception");
                }
            }
*/
        }
// !!!! ???? TODO : do something with MENU button ???? !!!!
// (either return true to 'stop' event, or show a DEBUG menu ?) 
/*
        else if (keyCode == KeyEvent.KEYCODE_MENU)
        {
            ...;
        }
*/
        else
        {
            result = super.onKeyDown(keyCode, event);
        }
        return result;
    }

    protected class StartActivityAfterAnimation implements Animation.AnimationListener
    {
        private Intent mIntent;

        StartActivityAfterAnimation(Intent intent)
        {
            mIntent = intent;
        }

        public void onAnimationEnd(Animation animation)
        {
            mButtonEasy.setVisibility(View.INVISIBLE);
            mButtonEasy.clearAnimation();
            mButtonMedium.setVisibility(View.INVISIBLE);
            mButtonMedium.clearAnimation();
            mButtonHard.setVisibility(View.INVISIBLE);
            mButtonHard.clearAnimation();
            if (mIntent != null)
                startActivity(mIntent);
            finish();

// !!!! ???? TODO : want to handle activity transitions ???? !!!!
/*
            if (UIConstants.mOverridePendingTransition != null)
            {
                try
                {
                    UIConstants.mOverridePendingTransition.invoke(DifficultyMenuActivity.this, R.anim.activity_fade_in, R.anim.activity_fade_out);
                }
                catch (InvocationTargetException ite)
                {
                    DebugLog.d("Activity Transition", "Invocation Target Exception");
                }
                catch (IllegalAccessException ie)
                {
                    DebugLog.d("Activity Transition", "Illegal Access Exception");
                }
            }
*/
        }

        public void onAnimationRepeat(Animation animation)
        {
        }

        public void onAnimationStart(Animation animation)
        {
        }
    }
}
