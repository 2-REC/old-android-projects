package derek.android.game.test;

// !!!! TODO : could be created as a inner public static class of the activity class !!!!
// (smaller class generated, accessible through GameActivity.PauseMenuView)


import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.AttributeSet;
import android.view.View;
import android.widget.RelativeLayout;


public class PauseMenuView extends RelativeLayout
{
    private View mButtonResume = null;
    private View mButtonSave = null;
//    private View mButtonSettings = null;
    private View mButtonQuit = null;

// !!!! ???? TO DO : buttons animations ???? !!!!
// => probably not, as want to be fast here ... (= no transitions)
//...

    private Handler mHandler = null;


    public PauseMenuView(Context context, AttributeSet attrs)
    {
        super(context, attrs);

// !!!! ???? TODO : need that ???? !!!!
        ((Activity)getContext()).getLayoutInflater().inflate(R.layout.pause_menu_layout, this);

        mButtonResume = findViewById(R.id.menu_pause_resume);
        mButtonSave = findViewById(R.id.menu_pause_save);
//        mButtonSettings = findViewById(R.id.menu_pause_settings);
        mButtonQuit = findViewById(R.id.menu_pause_quit);

        mButtonResume.setOnClickListener(mButtonResumeListener);
//        mButtonSave.setOnClickListener(mButtonSaveListener);
//        mButtonSettings.setOnClickListener(mButtonSettingsListener);
        mButtonQuit.setOnClickListener(mButtonQuitListener);
    }

// !!!! ???? TODO : need this or ok when done in constructor ? ???? !!!!
/*
    @Override
    protected void onFinishInflate()
    {
        super.onFinishInflate();
        ((Activity)getContext()).getLayoutInflater().inflate(R.layout.pause_menu_layout, this);
        ... (move code from constructor)
    }
*/

    public void setHandler(Handler handler)
    {
        mHandler = handler;
    }

    public void activate()
    {
//!!!! ???? TO DO : need that ???? !!!!
//?        mPauseMenu.setClickable(true);
//?        mPauseMenu.setFocusable(true);
        setVisibility(View.VISIBLE);
//        bringToFront();

// !!!! ???? TODO : check if ok with Quit dialog ???? !!!!
// (that not taking too long)
        // launch animations (if not ready)
        // ...
    }

    public void deactivate()
    {
//!!!! ???? TO DO : need that ???? !!!!
//?        mPauseMenu.setClickable(false);
//?        mPauseMenu.setFocusable(false);
        setVisibility(View.GONE);

        // launch animations (if not ready) ? (also anim when deactivating ?)
        // ...
    }

    private View.OnClickListener mButtonResumeListener = new View.OnClickListener()
    {
        public void onClick(View view)
        {

// !!!! ???? TO DO : buttons animations ???? !!!!
// => if yes, look at ActivityDifficultyMenu
//            view.startAnimation(mAnimationButton);

// !!!! ???? TODO : necessary ???? !!!!
// => done in View when receiving "pause" message
/*
// !!!! ???? TO DO : is this enough ? ???? !!!!
// (or need to set focusable & clickable ?)
            setVisibility(View.INVISIBLE);
*/
            if (mHandler != null)
            {
                Message msg = mHandler.obtainMessage();
// !!!! ???? TO DO : ok to use 'new' every time ? no memory leak ? ???? !!!!
                Bundle b = new Bundle();

//// STATE MSGS - begin
//                b.putBoolean("pause_resume", true);
//// STATE MSGS - mid
                b.putBoolean("pause_menu", true);
                b.putBoolean("unpause", true);
//// STATE MSGS - end

                msg.setData(b);
                mHandler.sendMessage(msg);
            }
        }
    };

/*
    private View.OnClickListener mButtonSaveListener = new View.OnClickListener()
    {
        public void onClick(View view)
        {
            Intent intent = new Intent(getBaseContext(), ActivitySaveGame.class);
// !!!! TO DO : send info to save ... !!!!
// => what ? how ?
//!            intent.putExtras(getIntent());
//!            intent.putExtra("difficulty", 0);
//...

// !!!! TO DO : check that game state is saved when launching other activity !!!!

// !!!! ???? TO DO : buttons animations ???? !!!!
// => if yes, look at ActivityDifficultyMenu
            startActivity(intent);
        }
    };
*/
/*
    private View.OnClickListener mButtonSettingsListener = new View.OnClickListener()
    {
        public void onClick(View view)
        {
            Intent intent = new Intent(getBaseContext(), ActivitySettings.class);
// !!!! ???? TO DO : something to send ???? !!!!
// => what ? how ?
//!            intent.putExtras(getIntent());
//!            intent.putExtra("difficulty", 0);
//...

// !!!! TO DO : check that game state is saved when launching other activity !!!!

// !!!! ???? TO DO : buttons animations ???? !!!!
// => if yes, look at ActivityDifficultyMenu
            startActivity(intent);
        }
    };
*/

    private View.OnClickListener mButtonQuitListener = new View.OnClickListener()
    {
        public void onClick(View view)
        {
// !!!! ???? TO DO : buttons animations ???? !!!!
// => if yes, look at ActivityDifficultyMenu

            if (mHandler != null)
            {
                Message msg = mHandler.obtainMessage();
// !!!! ???? TO DO : ok to use 'new' every time ? no memory leak ? ???? !!!!
                Bundle b = new Bundle();

//// STATE MSGS - begin
//                b.putBoolean("pause_exit", true);
//// STATE MSGS - mid
                b.putBoolean("pause_menu", true);
                b.putBoolean("quit", true);
//// STATE MSGS - end

                msg.setData(b);
                mHandler.sendMessage(msg);
            }
        }
    };
}
