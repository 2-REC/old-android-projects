package derek.android.game.test;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;


public class MainMenuActivity extends Activity
{
    private View mBackground;
    private View mTitle;

    private View mButtonNewGame;
    private View mButtonLoadGame;
    private View mButtonSettings;
    private View mButtonExit;

    private Animation mAnimationButton;
    private Animation mAnimationFadeOutBkg;
    private Animation mAnimationFadeOut;
//    private Animation mAnimationFadeIn;

    private boolean mJustCreated;
    private boolean mPaused;


    private View.OnClickListener mButtonNewGameListener = new View.OnClickListener()
    {
        public void onClick(View view)
        {
            if (!mPaused)
            {
                Intent intent = new Intent(getBaseContext(), DifficultyMenuActivity.class);
                intent.putExtra("new_game", true);
                view.startAnimation(mAnimationButton);
                mAnimationButton.setAnimationListener(new StartActivityAfterAnimation(intent));
                mPaused = true;
            }
        }
    };

// !!!! TODO : add activity ... !!!!
/*
    private View.OnClickListener mButtonLoadGameListener = new View.OnClickListener()
    {
        public void onClick(View view)
        {
            if (!mPaused)
            {
                Intent intent = new Intent(getBaseContext(), LoadGameActivity.class);
//?                intent.putExtra(..., true); // something to add ?
                view.startAnimation(mAnimationButton);
                mAnimationFadeOutBkg.setAnimationListener(new StartActivityAfterAnimation(intent));
                mBackground.startAnimation(mAnimationFadeOutBkg);
                mButtonNewGame.startAnimation(mAnimationFadeOut);
                mButtonSettings.startAnimation(mAnimationFadeOut);
                mButtonExit.startAnimation(mAnimationFadeOut);
                mPaused = true;
            }
        }
    };
*/

// !!!! TODO : add activity ... !!!!
/*
    private View.OnClickListener mButtonSettingsListener = new View.OnClickListener()
    {
        public void onClick(View view)
        {
            if (!mPaused)
            {
                Intent intent = new Intent(getBaseContext(), SettingsActivity.class);
// !!!! ???? TODO : could add a parameter to specify if in game or not (not, in this case) ???? !!!!
//?                intent.putExtra("ingame", false);
                view.startAnimation(mAnimationButton);
                mAnimationFadeOutBkg.setAnimationListener(new StartActivityAfterAnimation(intent));
                mBackground.startAnimation(mAnimationFadeOutBkg);
                mButtonNewGame.startAnimation(mAnimationFadeOut);
                mButtonLoadGame.startAnimation(mAnimationFadeOut);
                mButtonExit.startAnimation(mAnimationFadeOut);
                mPaused = true;
            }
        }
    };
*/

    private View.OnClickListener mButtonExitListener = new View.OnClickListener()
    {
        public void onClick(View view)
        {
            if (!mPaused)
            {
                view.startAnimation(mAnimationButton);
// !!!! ???? TODO : check if ok to pass null to StartActivityAfterAnimation ???? !!!!
                mAnimationFadeOutBkg.setAnimationListener(new StartActivityAfterAnimation(null));
                mBackground.startAnimation(mAnimationFadeOutBkg);
                mButtonNewGame.startAnimation(mAnimationFadeOut);
                mButtonLoadGame.startAnimation(mAnimationFadeOut);
                mButtonSettings.startAnimation(mAnimationFadeOut);
                mPaused = true;
            }
        }
    };


    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.menu_main);
        mPaused = true;

        mBackground = findViewById(R.id.background);
        mTitle = findViewById(R.id.title);
        
        mButtonNewGame = findViewById(R.id.button_newgame);
        mButtonLoadGame = findViewById(R.id.button_loadgame);
        mButtonSettings = findViewById(R.id.button_settings);
        mButtonExit = findViewById(R.id.button_exit);

        mButtonNewGame.setOnClickListener(mButtonNewGameListener);
//!        mButtonLoadGame.setOnClickListener(mButtonLoadGameListener);
//!        mButtonSettings.setOnClickListener(mButtonSettingsListener);
        mButtonExit.setOnClickListener(mButtonExitListener);

        mAnimationButton = AnimationUtils.loadAnimation(this, R.anim.button_flicker);
        mAnimationFadeOutBkg = AnimationUtils.loadAnimation(this, R.anim.fade_out);
        mAnimationFadeOut = AnimationUtils.loadAnimation(this, R.anim.fade_out);
//        mAnimationFadeIn = AnimationUtils.loadAnimation(this, R.anim.fade_in);

// !!!! TODO : preferences ... !!!!
//        SharedPreferences prefs = getSharedPreferences(PreferenceConstants.PREFERENCE_NAME, MODE_PRIVATE);


        mJustCreated = true;

// !!!! ???? TODO : need that ???? !!!!
//?        setVolumeControlStream(AudioManager.STREAM_MUSIC);

    }

    @Override
    protected void onPause()
    {
        super.onPause();
        mPaused = true;
    }

    @Override
    protected void onResume()
    {
        super.onResume();

// !!!! ???? TODO : shouldn't be done at end of function ? ???? !!!!
        mPaused = false;

        mAnimationButton.setAnimationListener(null);

        if (mBackground != null) {
        	mBackground.clearAnimation();
        }

        if (mJustCreated)
        {
            if (mButtonNewGame != null)
            {
                mButtonNewGame.startAnimation(AnimationUtils.loadAnimation(this, R.anim.button_slide));
            }
            if (mButtonLoadGame != null)
            {
                Animation anim = AnimationUtils.loadAnimation(this, R.anim.button_slide);
                anim.setStartOffset(500L);
                mButtonLoadGame.startAnimation(anim);
            }
            if (mButtonSettings != null)
            {
                Animation anim = AnimationUtils.loadAnimation(this, R.anim.button_slide);
                anim.setStartOffset(1000L);
                mButtonSettings.startAnimation(anim);
            }
            if (mButtonExit != null)
            {
                Animation anim = AnimationUtils.loadAnimation(this, R.anim.button_slide);
                anim.setStartOffset(1500L);
                mButtonExit.startAnimation(anim);
            }
            mJustCreated = false;
        }
        else
        {
            mButtonNewGame.clearAnimation();
            mButtonLoadGame.clearAnimation();
            mButtonSettings.clearAnimation();
            mButtonExit.clearAnimation();
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)
    {
        boolean result = true;
        if ((keyCode == KeyEvent.KEYCODE_BACK) || (keyCode == KeyEvent.KEYCODE_HOME))
        {
// !!!! ???? TODO : OK like this ???? !!!!
            mButtonExit.startAnimation(mAnimationButton);
// !!!! ???? TODO : check if ok to pass null to StartActivityAfterAnimation ???? !!!!
            mAnimationFadeOutBkg.setAnimationListener(new StartActivityAfterAnimation(null));
            mBackground.startAnimation(mAnimationFadeOutBkg);
            mButtonNewGame.startAnimation(mAnimationFadeOut);
            mButtonLoadGame.startAnimation(mAnimationFadeOut);
            mButtonSettings.startAnimation(mAnimationFadeOut);

// !!!! ???? TODO : want to handle activity transitions ???? !!!!
/*
            if (UIConstants.mOverridePendingTransition != null)
            {
                try
                {
                    UIConstants.mOverridePendingTransition.invoke(DifficultyMenuActivity.this, R.anim.activity_fade_in, R.anim.activity_fade_out);
                }
                catch (InvocationTargetException ite)
                {
                    DebugLog.d("Activity Transition", "Invocation Target Exception");
                }
                catch (IllegalAccessException ie)
                {
                    DebugLog.d("Activity Transition", "Illegal Access Exception");
                }
            }
*/
        }
// !!!! ???? TODO : do something with MENU button ???? !!!!
// (either return true to 'stop' event, or show a DEBUG menu ?) 
/*
        else if (keyCode == KeyEvent.KEYCODE_MENU)
        {
            ...;
        }
*/
        else
        {
            result = super.onKeyDown(keyCode, event);
        }
        return result;
    }

    protected class StartActivityAfterAnimation implements Animation.AnimationListener
    {
        private Intent mIntent;

        StartActivityAfterAnimation(Intent intent)
        {
            mIntent = intent;
        }

        public void onAnimationEnd(Animation animation)
        {
            if (mIntent != null)
            {
                startActivity(mIntent);
            }
            else
            {
// !!!! ???? TODO : OK ???? !!!!
                mButtonNewGame.setVisibility(View.INVISIBLE);
                mButtonNewGame.clearAnimation();
                mButtonLoadGame.setVisibility(View.INVISIBLE);
                mButtonLoadGame.clearAnimation();
                mButtonSettings.setVisibility(View.INVISIBLE);
                mButtonSettings.clearAnimation();
                mButtonExit.setVisibility(View.INVISIBLE);
                mButtonExit.clearAnimation();
// !!!! ???? TODO : something else to clean/free ???? !!!!
                finish();
            }

// !!!! ???? TODO : want to handle activity transitions ???? !!!!
/*
            if (UIConstants.mOverridePendingTransition != null)
            {
                try
                {
                    UIConstants.mOverridePendingTransition.invoke(MainMenuActivity.this, R.anim.activity_fade_in, R.anim.activity_fade_out);
                }
                catch (InvocationTargetException ite)
                {
                    DebugLog.d("Activity Transition", "Invocation Target Exception");
                }
                catch (IllegalAccessException ie)
                {
                    DebugLog.d("Activity Transition", "Illegal Access Exception");
                }
            }
*/
        }

        public void onAnimationRepeat(Animation animation)
        {
        }

        public void onAnimationStart(Animation animation)
        {
        }
    }
}
