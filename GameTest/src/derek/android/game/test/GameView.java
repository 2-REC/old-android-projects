package derek.android.game.test;

import android.content.Context;
import android.util.AttributeSet;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

//// Log init tag - begin
import android.util.Log;
//// Log init tag - end


public class GameView extends SurfaceView
                      implements SurfaceHolder.Callback
{
//// Log init tag - begin
    private static final String LOG_INIT_TAG = "LOG_INIT";
//// Log init tag - end

//// INIT - begin
    private GameActivity mGameActivity;
//// INIT - end

//?    private volatile GameThread mGameThread;
    private GameThread mGameThread;


    public GameView(Context context, AttributeSet attrs)
    {
        super(context, attrs);

//// INIT - begin
        mGameActivity = (GameActivity)context;
//// INIT - end

        SurfaceHolder holder = getHolder();
        holder.addCallback(this);
    }

    public void cleanup()
    {
        if (mGameThread != null)
        {
// !!!! ???? TODO : remove this and do it in Activity.onDestroy ???? !!!!
// !!!! TODO : make a separate function "stopGame" !!!!
            mGameThread.setRunning(false);
            mGameThread.cleanup();

// !!!! ???? TODO : need that here ???? !!!!
// => shouldn't be called in Activity ????
            // empty message queue of Handler
            removeCallbacks(mGameThread);
            mGameThread = null;
        }

//// INIT - begin
        mGameActivity = null;
//// INIT - end

//?        setOnTouchListener(null);

        SurfaceHolder holder = getHolder();
        holder.removeCallback(this);

//!!!! ???? TO DO : more to do ? ???? !!!!
//...
    }


    public void surfaceCreated(SurfaceHolder holder)
    {
//// Log init tag - begin
        Log.i(LOG_INIT_TAG, "GameView - surfaceCreated");
//// Log init tag - end

        if (mGameThread != null)
        {
//// Log init tag - begin
            Log.i(LOG_INIT_TAG, "GameView - surfaceCreated - (mGameThread != null)");
//// Log init tag - end

// !!!! ???? TO DO : can move it after the test ? ???? !!!!
//            mGameThread.setRunning(true);

            if (mGameThread.getState() == Thread.State.NEW)
            {
//// Log init tag - begin
                Log.i(LOG_INIT_TAG, "GameView - surfaceCreated -  (mGameThread.getState() == Thread.State.NEW)");
//// Log init tag - end

//// SIZES 20120725 - begin
/*
// !!!! TODO : should be moved to 'surfaceChanged' !!!!
                mGameThread.startGame();
*/
//// SIZES 20120725 - mid
            }
            else
            {
//// Log init tag - begin
                Log.i(LOG_INIT_TAG, "GameView - surfaceCreated -  (mGameThread.getState() != Thread.State.NEW)");
//// Log init tag - end
                // when have left the application and called surfaceDestroyed
                if (mGameThread.getState() == Thread.State.TERMINATED)
                {
//// Log init tag - begin
                    Log.i(LOG_INIT_TAG, "GameView - surfaceCreated -    (mGameThread.getState() == Thread.State.TERMINATED)");
//// Log init tag - end

                    mGameThread = new GameThread(this, mGameThread);

//// INIT - begin
                    mGameActivity.setThread(mGameThread);
//// INIT - end

//// SIZES 20120725 - begin
/*
// !!!! TODO : should be moved to 'surfaceChanged' !!!!
                    mGameThread.startGame();
*/
//// SIZES 20120725 - mid
//!!!! ???? TO DO : no need to 'delete' the old thread ? ???? !!!!
                }
// !!!! ???? TO DO : shouldn't do something if thread state is not TERMINATED ? ???? !!!!
// (other possible states : BLOCKED, RUNNABLE, TIMED_WAITING, WAITING)
                //else ... (could remove the "TERMINATED" test ?)
            }
//// SIZES 20120725 - begin
/*
//// INIT - begin
// !!!! TODO : should be moved to 'surfaceChanged' !!!!
// !!!! ???? TODO : this function could be fused with "mGameThread.startGame" ???? !!!!
            mGameThread.doStart();
//// INIT - end
*/
//// SIZES 20120725 - mid
        }
    }

    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height)
    {
//// Log init tag - begin
        Log.i(LOG_INIT_TAG, "GameView - surfaceChanged");
//// Log init tag - end

        if (mGameThread != null)
        {
            mGameThread.setSurfaceSize(width, height);
//// SIZES 20120725 - mid
// !!!! ???? TODO : OK here ? ???? !!!!
            mGameThread.startGame();
            mGameThread.doStart();
//// SIZES 20120725 - end
        }
    }

    public void surfaceDestroyed(SurfaceHolder arg0)
    {
//// Log init tag - begin
        Log.i(LOG_INIT_TAG, "GameView - surfaceDestroyed");
//// Log init tag - end

        boolean retry = true;
        if (mGameThread != null)
        {
//// Log init tag - begin
            Log.i(LOG_INIT_TAG, "GameView - surfaceDestroyed - (mGameThread != null)");
//// Log init tag - end

            mGameThread.setRunning(false);
        }
        while (retry)
        {
            try
            {
                if (mGameThread != null)
                {
                    mGameThread.join();
                }
                retry = false;
            }
            catch (InterruptedException e)
            {
//!!!! ???? TO DO : need to do something ???? !!!!
//?                mGameThread.interrupt();
            }
        }
    }


//!!!! TODO : could have all variables as public and avoid getters and setters ... !!!!
    public void setThread(GameThread newThread)
    {
        mGameThread = newThread;
    }
    public GameThread getThread()
    {
        return mGameThread;
    }


    @Override
    public void onWindowFocusChanged(boolean hasWindowFocus)
    {
// !!!! TODO : trace this function to see when called ... !!!!
        if(mGameThread != null)
        {
            if (!hasWindowFocus)
                mGameThread.pause();
        }
    }

}
