package derek.android.game.test;

import java.util.concurrent.ArrayBlockingQueue;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.KeyEvent;
import android.view.SurfaceHolder;


public class GameThread extends Thread
{
//// Logs - begin
    private static final String LOG_INIT_TAG = "LOG_INIT";
//// Logs - end

// !!!! ???? TODO : OK with size 30 ? ???? !!!!
    public static final int INPUT_QUEUE_SIZE = 30;

    public static final int STATE_UNSET = 0;
    public static final int STATE_INIT = 1;
    public static final int STATE_PAUSE = 2;
    public static final int STATE_RUNNING = 3;
    public static final int STATE_QUIT = 4;

// !!!! ???? TODO : OK ? or could set to INIT ? ???? !!!!
    private int mMode = STATE_UNSET;
    private boolean mRun = false;

    private SurfaceHolder mSurfaceHolder;
    private Handler mHandler;
    private Context mContext;

    public GameView mGameView;

    private ArrayBlockingQueue<InputObject> mInputQueue = new ArrayBlockingQueue<InputObject>(INPUT_QUEUE_SIZE);
    private Object mInputQueueMutex = new Object();

//// SIZES 20120725 - begin
    protected float mSizeFactorWidth = 1.0f;
    protected float mSizeFactorHeight = 1.0f;
// !!!! TODO : default sizes could (should?) be constants !!!!
    protected int mDefaultWidth = 800;
    protected int mDefaultHeight = 480;
// !!!! TODO : handle aspect ratio (if want to keep it - letterboxing, or distort it ...)
//    protected float mAspectRatio = 1.667f;
    protected int mCanvasWidth;
    protected int mCanvasHeight;
//// SIZES 20120725 - end

    protected long mLastTime = 0;

// !!!! TODO : add game related and sprites stuff !!!!
// => should have a list of objects ... (should be in another object ~GameObject ?)
//////// tmp game - begin
    private boolean mTouched;

    private int mDifficulty;

    protected Bitmap mBackgroundImage;

    Sprite mBall;
    Sprite mHole;
//////// tmp game - end


    public GameThread(GameView gameView)
    {
//// Logs - begin
        Log.i(LOG_INIT_TAG, "GameThread - GameThread(GameView gameView)");
//// Logs - end

        mMode = STATE_INIT;

        mGameView = gameView;
        mSurfaceHolder = gameView.getHolder();
        mContext = gameView.getContext();

//////// tmp game - begin
        mCanvasWidth = mDefaultWidth;
        mCanvasHeight = mDefaultHeight;

        mTouched = false;

        mBackgroundImage = BitmapFactory.decodeResource(mContext.getResources(),
                                                        R.drawable.background);

        mBall = new Sprite ("Ball", mContext, R.drawable.ball);
        mHole = new Sprite ("Hole", mContext, R.drawable.hole);
// !!!! ???? TODO : should be able to set the desired bitmaps sizes ? ???? !!!!
//...
//////// tmp game - end

    }

    public GameThread(GameView gameView, GameThread oldThread)
    {
//// Logs - begin
        Log.i(LOG_INIT_TAG, "GameThread - GameThread(GameView gameView, GameThread oldThread)");
//// Logs - end

        mGameView = gameView;
        mSurfaceHolder = gameView.getHolder();
        mContext = gameView.getContext();

// !!!! TODO : copy all the old values ... !!!!
//...

        mHandler = oldThread.mHandler;
// !!!! TODO : mode should be PAUSE or INIT !!!!
// (or READY ?)
        mMode = oldThread.mMode;
//// Logs - begin
        if (mMode == GameThread.STATE_PAUSE)
            Log.i(LOG_INIT_TAG, "GameThread - GameThread(GameView, GameThread) - mMode == PAUSE");
        else if (mMode == GameThread.STATE_INIT)
            Log.i(LOG_INIT_TAG, "GameThread - GameThread(GameView, GameThread) - mMode == INIT");
        else
            Log.i(LOG_INIT_TAG, "GameThread - GameThread(GameView, GameThread) - mMode != PAUSE - " + String.valueOf(mMode));
//// Logs - end

        mCanvasWidth = oldThread.mCanvasWidth;
        mCanvasHeight = oldThread.mCanvasHeight;
// !!!! ???? TODO : need to restore lastTime or should get a new one ???? !!!!
        mLastTime = oldThread.mLastTime;

//////// tmp game - begin
        mDifficulty = oldThread.mDifficulty;

        mTouched = false;

        mBackgroundImage = oldThread.mBackgroundImage;

// !!!! ???? TODO : OK like that ???? !!!!
        mBall = oldThread.mBall;
        mHole = oldThread.mHole;
//////// tmp game - end

// !!!! ???? TODO : no need to 'delete/release' the oldThread ? ???? !!!!
    }

    public void cleanup()
    {
        mContext = null;
        mGameView = null;
        mHandler = null;
        mSurfaceHolder = null;

// !!!! ???? TODO : anything else ???? !!!!

//////// tmp game - begin
// !!!! ???? TODO : need to free the Bitmaps ???? !!!!
        mBall = null;
        mHole = null;
//////// tmp game - end
    }

// !!!! TODO : init fields, depending on difficulty, level, ... !!!!
// !!!! ???? TODO : OK to forward Intent ???? !!!!
    public void initGame(Intent intent)
    {
//// Logs - begin
        Log.i(LOG_INIT_TAG, "GameThread - initGame");
//// Logs - end

// !!!! ???? TODO : should have a function that sets all the default variables ... ???? !!!!
//	        initGameState();

        if (intent.getBooleanExtra("new_game", false))
        {
//// Logs - begin
            Log.i(LOG_INIT_TAG, "GameThread - initGame - new_game");
//// Logs - end

            mDifficulty = intent.getIntExtra("difficulty", 1);
// !!!! TODO : set default values and stuff according to difficulty and settings ... !!!!
// (get some info from i.e. EasyDifficultyConstants.java, ...)
//...
        }
        else if (intent.getBooleanExtra("load_game", false))
        {
//// Logs - begin
            Log.i(LOG_INIT_TAG, "GameThread - initGame - load_game");
//// Logs - end

// !!!! TODO : find which saved game to load and load its info ... !!!!
// !!!! ???? TODO : use preferences ???? !!!!
// (or internal or external storage ????)
/*
            SharedPreferences prefs = getSharedPreferences(PreferenceConstants.PREFERENCE_NAME, MODE_PRIVATE);
            final boolean debugLogs = prefs.getBoolean(PreferenceConstants.PREFERENCE_ENABLE_DEBUG, false);
            mDifficulty = prefs.getInt(PreferenceConstants.PREFERENCE_DIFFICULTY, 1));
            m... = prefs.getInt(PreferenceConstants.PREFERENCE_..., 0);
*/
        }

//////// tmp game - begin
        mTouched = false;

// !!!! TODO : get it from a config file or something ... !!!!
        mBall.setX(mCanvasWidth / 2.0f);
        mBall.setY(mCanvasHeight * 0.75f);
// !!!! TODO : set display position in sprite class when setting position !!!!
        mBall.setDX(0.0f);
        mBall.setDY(0.0f);

        mHole.setX(mCanvasWidth / 2.0f);
        mHole.setY(0.0f);
// !!!! TODO : set display position in sprite class when setting position !!!!
        mHole.setDX(0.0f);
        mHole.setDY(mCanvasHeight / 10.0f);
        mHole.startMoveY(10.0f);
//////// tmp game - end

//// INIT - begin
// !!!! ???? TODO : could set state to READY ? ... ? ???? !!!!
// (else no need to change it, its supposed to be set to INIT)
// => SHOULD DISPLAY A "LOADING" MESSAGE UNTIL START PLAYING OR PAUSING !!!!
// (state should stay as "INIT" until start playing or pausing)
//// Logs - begin
        if (mMode == GameThread.STATE_PAUSE)
            Log.i(LOG_INIT_TAG, "GameThread - initGame - mMode == PAUSE");
        else if (mMode == GameThread.STATE_INIT)
            Log.i(LOG_INIT_TAG, "GameThread - initGame - mMode == INIT");
        else
            Log.i(LOG_INIT_TAG, "GameThread - initGame - mMode != PAUSE - " + String.valueOf(mMode));
//// Logs - end
//// INIT - end
    }

    public void startGame()
    {
        //setRunning(true);
        mRun = true;
        start();
    }

    public synchronized void restoreState(Bundle savedState)
    {
//// Logs - begin
        Log.i(LOG_INIT_TAG, "GameThread - restoreState");
//// Logs - end

        synchronized (mSurfaceHolder)
        {
// !!!! ???? TODO : should change the state to PAUSE if is RUNNING ???? !!!!
// (check all cases ...)
            mMode = savedState.getInt("mMode");
// => SHOULD DISPLAY A "LOADING" MESSAGE UNTIL START PLAYING OR PAUSING !!!!
// (state should stay as "INIT" until start playing or pausing)
//// Logs - begin
        if (mMode == GameThread.STATE_PAUSE)
            Log.i(LOG_INIT_TAG, "GameThread - restoreState - mMode == PAUSE");
        else if (mMode == GameThread.STATE_INIT)
            Log.i(LOG_INIT_TAG, "GameThread - restoreState - mMode == INIT");
        else
            Log.i(LOG_INIT_TAG, "GameThread - restoreState - mMode != PAUSE - " + String.valueOf(mMode));
//// Logs - end

// !!!! TODO : make sure all the variables are set/restored !!!!
// (some saved, some from DifficultyConstants, some from settings) 
//            mDifficulty = savedState.getInt(KEY_DIFFICULTY);
//...
// !!!! ???? TODO : should have a function that sets all the default variables ... ???? !!!!
//            initGameState();

//// SIZES 20120725 - mid
// !!!! ???? TODO : useless ? ???? !!!!
/*
// !!!! TODO : check that ok !!!!
            mCanvasWidth = savedState.getInt("mCanvasWidth");
            mCanvasHeight = savedState.getInt("mCanvasHeight");
            mSizeFactorWidth = mCanvasWidth / mDefaultWidth;
            mSizeFactorHeight = mCanvasHeight / mDefaultHeight;
*/
//// SIZES 20120725 - end

//////// tmp game - begin
            mTouched = false;

            mBall.restoreState(savedState);
            mHole.restoreState(savedState);
//////// tmp game - end

// !!!! TODO : may be needed !!!!
/*
            Integer gameMode = savedState.getInt("gameMode");
            if (gameMode == GameThread.STATE_LOSE
                || gameMode == GameThread.STATE_WIN)
            {
                setState(gameMode, true);
            }
*/
        }
    }

    public Bundle saveState(Bundle map)
    {
        synchronized (mSurfaceHolder)
        {
            if (map != null)
            {
// !!!! ???? TODO : if state is RUNNING, should set it to PAUSE ???? !!!!
                map.putInt("mMode", mMode);

//                map.putInt(KEY_DIFFICULTY, Integer.valueOf(mDifficulty));

//                map.putLong("score", score);
//                ...

// !!!! ???? TODO : useless ? ???? !!!!
/*
// !!!! TODO : check that ok !!!!
                map.putInt("mCanvasWidth", mCanvasWidth);
                map.putInt("mCanvasHeight", mCanvasHeight);
*/

//////// tmp game - begin
// !!!! ???? TODO : need to return "map" ? if yes, OK like this ? ???? !!!!
                map = mBall.saveState(map);
                map = mHole.saveState(map);
//////// tmp game - end


            }
        }
        return map;
    }

    public void doStart()
    {
        synchronized(mSurfaceHolder)
        {
// !!!! ???? TODO : OK here ???? !!!!
            mLastTime = System.currentTimeMillis() + 100;

//// Logs - begin
            Log.i(LOG_INIT_TAG, "GameThread - doStart");
            if (mMode == STATE_PAUSE)
                Log.i(LOG_INIT_TAG, "GameThread - doStart - (mMode == STATE_PAUSE)");
            else
                Log.i(LOG_INIT_TAG, "GameThread - doStart - (mMode != STATE_PAUSE) - " + String.valueOf(mMode));
//// Logs - end

// !!!! ???? TODO : see how it goes with other states ... ???? !!!!
            if (mMode == STATE_INIT)
                setState(STATE_RUNNING, true);
            // hack to display pause menu ...
            else if (mMode == STATE_PAUSE)
                setState(STATE_PAUSE, true);
        }
    }


    @Override
    public void run()
    {
        Canvas canvas;
        while (mRun)
        {
// !!!! ???? TODO : can test if (mMode == STATE_RUNNING) here ???? !!!!
// => and else case, have a "while(mMode == STATE_PAUSE) mPauseLock.wait()"
// => also, check if OK if processInput and doDraw are inside the running test !

// !!!! TODO : add a Log to check if in here even when on PAUSE !!!!
// (or even when out of Activity !)

            canvas = null;
            try
            {
                canvas = mSurfaceHolder.lockCanvas(null);
                synchronized (mSurfaceHolder)
                {
// !!!! ???? TODO : OK here ???? !!!!
                    processInput();
                    if (mMode == STATE_RUNNING)
                    {
                        updatePhysics();
                    }
                    doDraw(canvas);
                }
            }
            finally
            {
                if (canvas != null)
                {
                    if(mSurfaceHolder != null)
                        mSurfaceHolder.unlockCanvasAndPost(canvas);
                }
            }
        }
    }

    public void setSurfaceSize(int width, int height)
    {
//// Logs - begin
        Log.i(LOG_INIT_TAG, "GameThread - setSurfaceSize - width = " + String.valueOf(width));
        Log.i(LOG_INIT_TAG, "GameThread - setSurfaceSize - height = " + String.valueOf(height));
//// Logs - end

// !!!! ???? TODO : why in synchronised block ???? !!!!
        synchronized (mSurfaceHolder)
        {
//// SIZES 20120725 - mid
            if (width != mCanvasWidth)
                mSizeFactorWidth = width / mDefaultWidth;
            if (height != mCanvasHeight)
                mSizeFactorHeight = height / mDefaultHeight;
            mCanvasWidth = width;
            mCanvasHeight = height;

            if ( (mSizeFactorWidth != 1.0f) || (mSizeFactorHeight != 1.0f) )
            {
//// Logs - begin
                Log.i(LOG_INIT_TAG, "GameThread - setSurfaceSize - CHANGING SIZE");
//// Logs - end
//// SIZES 20120725 - end


// !!!! TODO : should call a "applyNewSize" function !!!!

////// tmp game - begin
// !!!! TODO : need to scale all the images !!!!
                mBackgroundImage = Bitmap.createScaledBitmap(mBackgroundImage, width, height, true);

                // set correct display sizes
                mBall.updateDisplaySize(mSizeFactorWidth, mSizeFactorHeight);
                mHole.updateDisplaySize(mSizeFactorWidth, mSizeFactorHeight);

//// SIZES 20120725 - mid
            }
//// SIZES 20120725 - end

//////// tmp game - end
        }
    }

    protected void doDraw(Canvas canvas)
    {
        if(canvas == null)
            return;

// !!!! TODO : draw resources ... !!!!
//...

//////// tmp game - begin
// !!!! ???? TODO : need to check if background not null ???? !!!!
// => if it is, its wrong anyway ...
        if(mBackgroundImage != null)
            canvas.drawBitmap(mBackgroundImage, 0, 0, null);

        mBall.doDraw (canvas);
        mHole.doDraw (canvas);
//////// tmp game - end
    }

    protected void updatePhysics()
    {
        long now = System.currentTimeMillis();

        float elapsed = (now - mLastTime) / 1000.0f;


//////// tmp game - begin

// !!!! TODO : movements should be done in the sprites respective classes !!!!
// => mBall.update(elapsed);
// => need to take account of touched, canvas size, elapsed time, etc. !
// (=> sprites need to know the size of the canvas !!!!)
        mBall.update(elapsed);
        mHole.update(elapsed);

// !!!! TODO : use a "border" and not "0" !!!!
        if ((mBall.getX() <= 0 && mBall.getDX() < 0)
            || (mBall.getX() >= mCanvasWidth - mBall.getBitmap().getWidth() && mBall.getDX() > 0) )
        {
//            mBall.setDX(0.0f);
            mBall.stopMoveX();
        }
//        if ((mBallY <= 0 && mBallDY < 0)
//            || (mBallY >= mCanvasHeight - mBall.getHeight() && mBallDY > 0) )
//        {
//            mBallDY = -mBallDY;
//        }

        if ( mHole.getY() >= mCanvasHeight )
        {
// !!!! TODO : put the hole at a random X !!!!
//...
            mHole.setY(0.0f);
        }
//////// tmp game - end

        mLastTime = now;
    }


    public void feedInput(InputObject input)
    {
        synchronized(mInputQueueMutex)
        {
            try
            {
                mInputQueue.put(input);
            }
            catch (InterruptedException e)
            {
            }
        }
    }

    private void processInput()
    {
        synchronized(mInputQueueMutex)
        {
            ArrayBlockingQueue<InputObject> inputQueue = mInputQueue;
            while (!inputQueue.isEmpty())
            {
                try
                {
                    InputObject input = inputQueue.take();
                    if (input.mEventType == InputObject.EVENT_TYPE_KEY)
                    {
                        processKeyEvent(input);
                    }
                    else if (input.mEventType == InputObject.EVENT_TYPE_TOUCH)
                    {
                        processMotionEvent(input);
                    }
//// Sensors - begin
/*
// !!!! ???? TODO : SensorEvent - OK to handle as other inputs ???? !!!!
                    else if (input.mEventType == InputObject.EVENT_TYPE_SENSOR)
                    {
                        processOrientationEvent(input);
                    }
*/
//// Sensors - end
                    input.returnToPool();
                }
                catch (InterruptedException e)
                {
                }
            }
        }
    }

    private void processMotionEvent(InputObject input)
    {
// !!!! TODO : handle touch input ... !!!!

// !!!! ???? TODO : should test the game state ???? !!!!
// => maybe not required here, as the PauseMenuView is on top ?
//?        if (mMode == STATE_RUNNING)

        if (input.mAction == InputObject.ACTION_TOUCH_DOWN)
        {
// !!!! ???? TODO : use variables for mCanvasWidth/2 ? ???? !!!!
// (to avoid divisions ...)
            mTouched = true;

// !!!! TODO : should change "true/false" to "left/right" !!!!
            if (input.mX > mCanvasWidth / 2)
                mBall.startMoveX(10.0f);
            else
                mBall.startMoveX(-10.0f);
        }
/*
        else if (input.mAction == InputObject.ACTION_TOUCH_MOVE)
        {
        ...
        }
*/
        else if (input.mAction == InputObject.ACTION_TOUCH_UP)
        {
            mTouched = false;

            mBall.endMoveX();
        }
    }

    private void processKeyEvent(InputObject input)
    {
// !!!! ???? TODO : handle other keys input ... ???? !!!!
        if (input.mAction == InputObject.ACTION_KEY_DOWN)
        {
            // pause / unpause
            if (input.mKeyCode == KeyEvent.KEYCODE_MENU)
            {
// !!!! TODO : check if ok when have other states !!!
// (should handle other states transitions no ?)
                if (mMode == STATE_RUNNING)
                {
                    setState(STATE_PAUSE, true);
                }
                else
                {
                    unpause(true);
                }
            }
            // quit dialog
            else if (input.mKeyCode == KeyEvent.KEYCODE_BACK)
            {
// !!!! TODO : could call "quit" ... !!!!
                setState(STATE_QUIT, true);
            }
        }
        else if (input.mAction == InputObject.ACTION_KEY_UP)
        {
            //...
        }
    }

//// Sensors - begin
/*
// !!!! ???? TODO : SensorEvent - OK to handle as other inputs ???? !!!!
//    public boolean onOrientationEvent(float x, float y, float z)
//    {
//        if (mMode == STATE_RUNNING)
//        {
//            //...
//        }
//        return true;
//    }
    private void processOrientationEvent(InputObject input)
    {
// !!!! ???? TODO : should test the game state ???? !!!!
        if (mMode == STATE_RUNNING)
        {
//// Logs - begin
            Log.i(LOG_INIT_TAG, "ORIENTATION");
            Log.i(LOG_INIT_TAG, "ORIENTATION - X = " + String.valueOf(input.mX));
            Log.i(LOG_INIT_TAG, "ORIENTATION - Y = " + String.valueOf(input.mY));
//// Logs - end
            if (input.mAction == InputObject.ACTION_ORIENTATION)
            {
//?                mTouched = true;
// !!!! TODO : check orientations !!!!
                if (input.mX > 0)
                {
//// Logs - begin
                    Log.i(LOG_INIT_TAG, "ORIENTATION - POSITIVE");
//// Logs - end
                    mBallDX = 10;
                }
                else if (input.mX < 0)
                {
//// Logs - begin
                    Log.i(LOG_INIT_TAG, "ORIENTATION - NEGATIVE");
//// Logs - end
                    mBallDX = -10;
                }
                //? mY ?
            }
        }
    }
*/
//// Sensors - end


    public void pause()
    {
// !!!! ???? TODO : why 'synchronized' ???? !!!!
// (its done in setState)
//?        synchronized (mSurfaceHolder)
//?        {
            if (mMode == STATE_RUNNING)
                setState(STATE_PAUSE, true);
//?        }
    }

    public void unpause (boolean msg)
    {
// !!!! ???? TODO : why 'synchronized' ???? !!!!
        synchronized (mSurfaceHolder)
        {
            mLastTime = System.currentTimeMillis();
        }
        setState(STATE_RUNNING, msg);
    }

    public void quit (boolean msg)
    {
        setState(STATE_QUIT, msg);
    }

    public void setState(int mode, boolean msg)
    {
//// Logs - begin
        Log.i(LOG_INIT_TAG, "GameThread - setState");
//// Logs - end
        synchronized (mSurfaceHolder)
        {
            mMode = mode;
            if (mMode == STATE_RUNNING)
            {
//// Logs - begin
                Log.i(LOG_INIT_TAG, "GameThread - setState - RUNNING");
//// Logs - end
                if ( msg && (mHandler != null) )
                {
                    Message message = mHandler.obtainMessage();
// !!!! ???? TODO : ok to use 'new' every time ? no memory leak ? ???? !!!!
                    Bundle b = new Bundle();

                    b.putBoolean("unpause", true);
// !!!! ???? TODO : need to change the 'clickable' statuses of buttons ? ???? !!!!

                    message.setData(b);
                    mHandler.sendMessage(message);
                }

// !!!! ???? TODO : something else to do ? ???? !!!!
//...

            }
// !!!! ???? TODO : OK ???? !!!!
            else if (mMode == STATE_PAUSE)
            {
//// Logs - begin
                Log.i(LOG_INIT_TAG, "GameThread - setState - PAUSE");
//// Logs - end
                if ( msg && (mHandler != null) )
                {
                    Message message = mHandler.obtainMessage();
// !!!! ???? TODO : ok to use 'new' every time ? no memory leak ? ???? !!!!
// => not better to use a global variable ?
                    Bundle b = new Bundle();

                    b.putBoolean("pause", true);

                    message.setData(b);
                    mHandler.sendMessage(message);
                }
            }
            else if (mMode == STATE_QUIT)
            {
//// Logs - begin
                Log.i(LOG_INIT_TAG, "GameThread - setState - QUIT");
//// Logs - end
                if ( msg && (mHandler != null) )
                {
                    Message message = mHandler.obtainMessage();
                    Bundle b = new Bundle();
                    b = new Bundle();
                    b.putBoolean("quit", true);

                    message.setData(b);
                    mHandler.sendMessage(message);
//?                    b = null;
//?                    msg = null;
                }

// !!!! ???? TODO : should set it back the state it was before, except if was RUNNING ? ???? !!!!
                mMode = STATE_PAUSE;
            }
/*
            else if (mMode == STATE_INIT)
            {
//// Logs - begin
                Log.i(LOG_INIT_TAG, "GameThread - setState - INIT");
//// Logs - end
            }
*/
// !!!! ???? TODO : other states ???? !!!!
// (WIN, LOSE, ... ?)
            else
            {
//// Logs - begin
                Log.i(LOG_INIT_TAG, "GameThread - setState - else ... ?");
//// Logs - end
// !!!! TODO : may need to stop stuff, or set things to 0 !!!!
// (+ bundle messages, etc ...)
//...
            }
        }
    }

// !!!! TODO : save the game ... !!!!
// ???? => what to save ? where ? how ? ????
// ???? do that here ???? (how to interact with SaveActivity ????)
    protected void saveGame()
    {
// !!!! TODO : save data ... !!!!
/*
        if (mPrefsEditor != null)
        {
            final int completed = LevelTree.packCompletedLevels(mLevelRow);
            ...
            mPrefsEditor.putInt(PreferenceConstants.PREFERENCE_DIFFICULTY, mDifficulty);
            mPrefsEditor.commit();
        }
*/
    }


// !!!! ???? TODO : need all these functions ???? !!!!
    public void setSurfaceHolder(SurfaceHolder h) {
        mSurfaceHolder = h;
    }

    public boolean isRunning() {
        return mRun;
    }
    public void setRunning(boolean running)
    {
        mRun = running;
    }

    public int getMode()
    {
        return mMode;
    }

    public void setMode(int mode)
    {
        mMode = mode;
    }

    public void setHandler(Handler handler)
    {
    	mHandler = handler;
    }

/*
    public boolean isPaused()
    {
        if (mMode == STATE_PAUSE)
            return true;
        else
            return false;
    }
*/

}
