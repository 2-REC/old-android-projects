package derek.android.game.test;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.os.Bundle;
//// Logs - begin
import android.util.Log;
//// Logs - end


public class Sprite
{
//// Logs - begin
    private static final String LOG_INIT_TAG = "LOG_INIT";
//// Logs - end

    private String mID;
    private Bitmap mBitmap;
    private float mX;
    private float mY;
    private float mDX = 0.0f;
    private float mDY = 0.0f;

    private int mWidth;
    private int mHeight;

    private float mSizeFactorWidth = 1.0f;
    private float mSizeFactorHeight = 1.0f;
    private float mDisplayX;
    private float mDisplayY;
    private int mDisplayWidth;
    private int mDisplayHeight;

    private boolean mMoveX = false;
    private boolean mMoveY = false;


    public Sprite (String name, Context context, int id)
    {
        mID = name;

        mX = 0.0f;
        mY = 0.0f;
        mDX = 0.0f;
        mDY = 0.0f;

        mDisplayX = 0.0f;
        mDisplayY = 0.0f;

        BitmapFactory.Options options = new BitmapFactory.Options();
        mBitmap = BitmapFactory.decodeResource (context.getResources(), id, options);

        mWidth = (int)(mBitmap.getWidth() * (options.inDensity / (float)(options.inTargetDensity)));
        mHeight = (int)(mBitmap.getHeight() * (options.inDensity / (float)(options.inTargetDensity)));
        mDisplayWidth = mWidth;
        mDisplayHeight = mHeight;

        mBitmap = Bitmap.createScaledBitmap(mBitmap,
                                            mDisplayWidth,
                                            mDisplayHeight,
                                            true);
//// Logs - begin
        Log.i(LOG_INIT_TAG, "Sprite - width   : " + String.valueOf(mWidth));
        Log.i(LOG_INIT_TAG, "Sprite - height  : " + String.valueOf(mHeight));
        Log.i(LOG_INIT_TAG, "Sprite - inTargetDensity : " + String.valueOf(options.inTargetDensity));
        Log.i(LOG_INIT_TAG, "Sprite - inDensity : " + String.valueOf(options.inDensity));
//// Logs - end
    }

    protected void doDraw(Canvas canvas)
    {
        canvas.drawBitmap(mBitmap, mDisplayX, mDisplayY, null);
    }

// !!!! TODO : could have a "time" parameter to specify the elapsed time !!!!
// !!!! TODO : should have a friction variable to update the velocities !!!!
//// intelligent sprites 2012/08/02 - begin
/*
    public void update()
    {
        mX += mDX;
        mY += mDY;
    }
*/
//// intelligent sprites 2012/08/02 - mid
    public void update(float elapsed)
    {
        if (mMoveX)
        {
            mDX *= 1.1f;
            if (Math.abs(mDX) > 50.0f)
                mDX = Math.signum(mDX) * 50.0f;
        }
        else
        {
            mDX = mDX * 0.9f;
            if (Math.abs(mDX) < 0.1f)
                mDX = 0.0f;
        }
        mX += mDX * elapsed;
        mDisplayX = mX * mSizeFactorWidth;

        if (mMoveY)
        {
            mDY *= 1.1f;
            if (Math.abs(mDY) > 50.0f)
                mDY = Math.signum(mDY) * 50.0f;
        }
        else
        {
            mDY = mDY * 0.9f;
            if (Math.abs(mDY) < 0.1f)
                mDY = 0.0f;
        }
        mY += mDY * elapsed;
        mDisplayY = mY * mSizeFactorHeight;

        Log.i(LOG_INIT_TAG, "Sprite - update - mX : " + String.valueOf(mX));
        Log.i(LOG_INIT_TAG, "Sprite - update - mY : " + String.valueOf(mY));
        Log.i(LOG_INIT_TAG, "Sprite - update - mDX : " + String.valueOf(mDX));
        Log.i(LOG_INIT_TAG, "Sprite - update - mDY : " + String.valueOf(mDY));
    }
// !!!! TODO : change !!!!
// => should have a more smooth/subtle movement
    public void startMoveX(float x)
    {
        Log.i(LOG_INIT_TAG, "Sprite - startMoveX - x : " + String.valueOf(x));
        mMoveX = true;
        mDX = x;
    }
    public void endMoveX()
    {
        Log.i(LOG_INIT_TAG, "Sprite - endMoveX ");
        mMoveX = false;
    }
    public void stopMoveX()
    {
        Log.i(LOG_INIT_TAG, "Sprite - stopMoveX ");
        mDX = 0.0f;
        mMoveX = false;
    }
 // !!!! TODO : change !!!!
// => should have a more smooth/subtle movement
    public void startMoveY(float y)
    {
        mMoveY = true;
        mDY = y;
    }
    public void endMoveY()
    {
        mMoveY = false;
    }
    public void stopMoveY()
    {
        mDY = 0.0f;
        mMoveY = false;
    }
//// intelligent sprites 2012/08/02 - end

    public void updateDisplaySize (float sizeFactorWidth, float sizeFactorHeight)
    {
        mSizeFactorWidth = sizeFactorWidth;
        mSizeFactorHeight = sizeFactorHeight;

        mDisplayX = mX * mSizeFactorWidth;
        mDisplayY = mY * mSizeFactorHeight;
        mDisplayWidth = (int)(mWidth * mSizeFactorWidth);
        mDisplayHeight = (int)(mHeight * mSizeFactorHeight);

        mBitmap = Bitmap.createScaledBitmap(mBitmap,
                                            mDisplayWidth,
                                            mDisplayHeight,
                                            true);
    }

// !!!! ???? TODO : need to return "map" ? if yes, OK like this ? ???? !!!!
    public Bundle saveState(Bundle map)
    {
//// Logs - begin
        Log.i(LOG_INIT_TAG, "Sprite - saveState");
//// Logs - end
        map.putFloat(mID + "X", mX);
        map.putFloat(mID + "Y", mY);
        map.putFloat(mID + "DX", mDX);
        map.putFloat(mID + "DY", mDY);
        return map;
    }
    public void restoreState (Bundle savedState)
    {
//// Logs - begin
        Log.i(LOG_INIT_TAG, "Sprite - restoreState");
//// Logs - end
        mX = savedState.getFloat(mID + "X");
        mY = savedState.getFloat(mID + "Y");
        mDX = savedState.getFloat(mID + "DX");
        mDY = savedState.getFloat(mID + "DY");
    }

    public String getID()
    {
        return mID;
    }
    public Bitmap getBitmap()
    {
        return mBitmap;
    }

    public void setX (float x)
    {
        mX = x;
        mDisplayX = mX * mSizeFactorWidth;
    }
    public float getX()
    {
        return mX;
    }

    public void setY (float y)
    {
        mY = y;
        mDisplayY = mY * mSizeFactorHeight;
    }
    public float getY()
    {
        return mY;
    }

    public void setDX (float dX)
    {
        mDX = dX;
    }
    public float getDX()
    {
        return mDX;
    }

    public void setDY (float dY)
    {
        mDY = dY;
    }
    public float getDY()
    {
        return mDY;
    }
}
